local moduleName = ...
local M = {}
_G[moduleName] = M 
M.sda=4
M.scl=3
 
local function sd(val)
if (val==0 or val==nil) then
gpio.write(M.sda, gpio.LOW)
gpio.mode(M.sda, gpio.OUTPUT)
else
gpio.mode(M.sda, gpio.INPUT)
gpio.write(M.sda, gpio.HIGH)
end end
 
local function sc(val)
if (val==0 or val==nil) then
gpio.write(M.scl, gpio.LOW)
else
gpio.write(M.scl, gpio.HIGH)
end end
 
local function dr()
gpio.mode(M.sda, gpio.INPUT)
return gpio.read(M.sda)
end
 
local function wait()
for i = 1, 100 do
 tmr.delay(10000)
 if dr() == gpio.LOW then
  return true
 end
end
return false
end
 
local function read(bits)
local val = 0
 for i = 0, bits-1 do
  sc(1)
  val = val * 2 + dr()
  sc(0)
 end
return val
end
 
local function write(val,bits)
 bits = bits -1
 for i = 0, bits do
 if bit.band(val, 2 ^ (bits-i)) == 0 then
  sd(0)
 else
  sd(1)
 end
  sc(1); sc(0)
 end
end
 
local function read_cmd(cmd)
 sd(1) sc(1) sd(0) sc(0) sc(1) sd(1) sc(0)
 write(cmd,8)
 sc(1); sc(0)
 if not wait() then
  return nil
 end
 sd(1)
 local val = read(8)
 sd(1) sd(0) sc(1) sc(0) sd(1)
 val = val * 256 + read(8) 
 sd(1) sc(1) sc(0)
 return val
end
 
function M.init(d, l)
 if d ~= nil then
  sda = d
 end
 if l ~= nil then
  scl = l
 end
 gpio.mode(sda, gpio.INPUT)
 gpio.mode(scl, gpio.OUTPUT)
 print("SHT1x init done SDA",sda,"SCL",scl)
end
 
function M.get_raw_temperature()
 return read_cmd(3)
end
 
function M.get_raw_humidity()
 return read_cmd(5)
end
 
return M
