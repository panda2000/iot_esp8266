    /*  
     * FreeModbus Libary: Win32 Port  
     * Copyright (C) 2006 Christian Walter <wolti@sil.at>  
     *  
     * This library is free software; you can redistribute it and/or  
     * modify it under the terms of the GNU Lesser General Public  
     * License as published by the Free Software Foundation; either  
     * version 2.1 of the License, or (at your option) any later version.  
     *  
     * This library is distributed in the hope that it will be useful,  
     * but WITHOUT ANY WARRANTY; without even the implied warranty of  
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU  
     * Lesser General Public License for more details.  
     *  
     * You should have received a copy of the GNU Lesser General Public  
     * License along with this library; if not, write to the Free Software  
     * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  
     *  
     * File: $Id: porttcp.c,v 1.1 2007/09/12 10:15:56 wolti Exp $  
     */   
       
    /*  
     * Design Notes:  
     *  
     * The xMBPortTCPInit function allocates a socket and binds the socket to  
     * all available interfaces ( bind with INADDR_ANY ). In addition it  
     * creates an array of event objects which is used to check the state of  
     * the clients. On event object is used to handle new connections or  
     * closed ones. The other objects are used on a per client basis for  
     * processing.  
     */   
       
     /**********************************************************  
     *  esp8266 TCP support.  
     *  Based on Walter's project.   
     *  Modified by Max Semin <makc_s_a@mail.ru>  
     ***********************************************************/   
       
//    #include <stdio.h>   
//    #include <sys/types.h>   
//    #include <sys/socket.h>   
//    #include <string.h>   
//    #include <netinet/in.h>   
//    #include <unistd.h>   
//    #include <errno.h>   
//    #include <time.h>   
    
#include "osapi.h"    
#include "port.h"
#include "porttcp.h"
//#include <ip_addr.h>
//#include <c_types.h>
//#include <espconn.h>   
       
/* ----------------------- Modbus includes ----------------------------------*/   
#include "mb.h"   
#include "mbport.h"   
       
       
       
/* ----------------------- MBAP Header --------------------------------------*/   
#define MB_TCP_UID          6   
#define MB_TCP_LEN          4   
#define MB_TCP_FUNC         7   
       
/* ----------------------- Defines  -----------------------------------------*/   
#define MB_TCP_DEFAULT_PORT 502 /* TCP listening port. */   
#define MB_TCP_POOL_TIMEOUT 50  /* pool timeout for event waiting. */   
#define MB_TCP_READ_TIMEOUT 1000        /* Maximum timeout to wait for packets. */   
#define MB_TCP_READ_CYCLE   100 /* Time between checking for new data. */   
       
#define MB_TCP_DEBUG        1   /* Set to 1 for additional debug output. */   
       
#define MB_TCP_BUF_SIZE     ( 256 + 7 ) /* Must hold a complete Modbus TCP frame. */   
       
#define EV_CONNECTION       0   
#define EV_CLIENT           1   
#define EV_NEVENTS          EV_CLIENT + 1   
       
/* ----------------------- Static variables ---------------------------------*/   
/*
SOCKET          xListenSocket;   
SOCKET          xClientSocket = INVALID_SOCKET;   
static fd_set   allset;   
*/       
static UCHAR    aucTCPBuf[MB_TCP_BUF_SIZE];   
static USHORT   usTCPBufPos;   
static USHORT   usTCPFrameBytesLeft;   

static struct espconn serverConn;
static esp_tcp serverTcp;

//Connection pool
static char txbuffer [MB_TCP_BUF_SIZE];
serverConnData connData;
/* ----------------------- External functions -------------------------------*/   
CHAR           *WsaError2String( int dwError );   
       
/* ----------------------- Static functions ---------------------------------*/   
//BOOL            prvMBTCPPortAddressToString( SOCKET xSocket, CHAR * szAddr, USHORT usBufSize );   
CHAR           *prvMBTCPPortFrameToString( UCHAR * pucFrame, USHORT usFrameLen );   
BOOL	     	prvbMBPortAcceptClient( void *arg );   
void     	prvvMBPortReleaseClient( void );   
       
static void serverConnectCb(void *arg);
static void ICACHE_FLASH_ATTR serverReconCb(void *arg, sint8 err);

       
/* ----------------------- Begin implementation -----------------------------*/   
       
BOOL   
xMBTCPPortInit( USHORT usTCPPort )   
{
	connData.conn = NULL;
	connData.txbuffer = txbuffer;
	connData.txbufferlen = 0;
	connData.readytosend = true;
	
	serverConn.type=ESPCONN_TCP;
	serverConn.state=ESPCONN_NONE;
	serverTcp.local_port=usTCPPort;
	serverConn.proto.tcp=&serverTcp;

	espconn_regist_connectcb(&serverConn, serverConnectCb);
	espconn_accept(&serverConn);
	espconn_regist_time(&serverConn, SERVER_TIMEOUT, 0);
        return TRUE;   
    }   
       
void   
vMBTCPPortClose(  )   
{   
	
        // Close all client sockets.    
  //      if( xClientSocket != SOCKET_ERROR )   
  //      {   
            prvvMBPortReleaseClient(  );   
  //      }   
        // Close the listener socket.   
	/*
        if( xListenSocket != SOCKET_ERROR )   
        {   
            close( xListenSocket );   
        } 
	*/
}   
       
void   
vMBTCPPortDisable( void )   
{   
        /* Close all client sockets. */   
    //    if( xClientSocket != SOCKET_ERROR )   
    //    {   
            prvvMBPortReleaseClient(  );   
    //    }   
}   
          
    /*!  
     * \ingroup port_win32tcp  
     * \brief Receives parts of a Modbus TCP frame and if complete notifies  
     *    the protocol stack.  
     * \internal   
     *  
     * This function reads a complete Modbus TCP frame from the protocol stack.  
     * It starts by reading the header with an initial request size for  
     * usTCPFrameBytesLeft = MB_TCP_FUNC. If the header is complete the   
     * number of bytes left can be calculated from it (See Length in MBAP header).  
     * Further read calls are issued until the frame is complete.  
     *  
     * \return \c TRUE if part of a Modbus TCP frame could be processed. In case  
     *   of a communication error the function returns \c FALSE.  
     */   
       
BOOL   
xMBTCPPortGetRequest( UCHAR ** ppucMBTCPFrame, USHORT * usTCPLength )   
{   
    *ppucMBTCPFrame = &aucTCPBuf[0];   
    *usTCPLength = usTCPBufPos;   
   
    /* Reset the buffer. */   
    usTCPBufPos = 0;   
    usTCPFrameBytesLeft = MB_TCP_FUNC;   
    return TRUE;   
}  
       
BOOL   
xMBTCPPortSendResponse( const UCHAR * pucMBTCPFrame, USHORT usTCPLength )   
{   
	
        BOOL            bFrameSent = FALSE;   
        BOOL            bAbort = FALSE;   
        int             res;   
        int             iBytesSent = 0;   
        int             iTimeOut = MB_TCP_READ_TIMEOUT;   
	
	//os_printf("xMBTCPPortSendResponse: %d\n", usTCPLength);
	//os_printf("pucMBTCPFrame: %d %d %d %d %d %d %d %d %d %d %d %d  \n", pucMBTCPFrame[0], pucMBTCPFrame[1], pucMBTCPFrame[2], pucMBTCPFrame[3], pucMBTCPFrame[4], pucMBTCPFrame[5], pucMBTCPFrame[6], pucMBTCPFrame[7], pucMBTCPFrame[8], pucMBTCPFrame[9], pucMBTCPFrame[10], pucMBTCPFrame[11]);
	espconn_sent(&serverConn, (uint8 *) pucMBTCPFrame, (uint16) usTCPLength);

        return TRUE;
}   
       
void   
prvvMBPortReleaseClient(  )   
{   
        if (connData.conn->state==ESPCONN_NONE || connData.conn->state==ESPCONN_CLOSE) 
		connData.conn=NULL;   
}   
      
BOOL   
prvbMBPortAcceptClient( void *arg )   
{
	return TRUE;
}	
//----------------------------------------------------------------------------

static void ICACHE_FLASH_ATTR serverDisconCb(void *arg) {
	//Just look at all the sockets and kill the slot if needed.
	if (connData.conn!=NULL) 
		if (connData.conn->state==ESPCONN_NONE || connData.conn->state==ESPCONN_CLOSE) 
			connData.conn=NULL;
}

//callback after the data are sent
static void ICACHE_FLASH_ATTR serverSentCb(void *arg) {
	struct espconn *conn = arg;
	os_printf("serverSentCb on conn %p\n", conn);

	espconn_disconnect(conn);
}

static void ICACHE_FLASH_ATTR serverRecvCb(void *arg, char *data, unsigned short len) {
	int x;
	char *p, *e;
	char receive [MAX_TCP_BUFF];
	char * rec = &receive[0];
	struct espconn *conn = arg;
	if (conn == NULL) return;
	os_printf("Receive callback on conn: %p   len: %u\n", conn, len);
	//os_printf("Receive data: %d %d %d %d %d %d %d %d %d %d %d %d  \n", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11]);
	
	int i;
	for(i = 0; i < MAX_TCP_BUFF; i++) {
	    receive [i]='\0';
        }
        for(i = 0; i < len; i++) {
	    DBG1 ("0x");
            DBG_H (data[i]);
	    DBG1 (" ");
        }
	DBG1 ("\n");
	DBG1 ("\nLen = ");
	DBG16 (len);
	DBG1 ("\n");
	
	Poll(data, rec , &len);
	DBG1 ("\nRecive Len = ");
	DBG16 (len);
	DBG1 ("\nRecive data = ");
	for(i = 0; i < len; i++) {
	    DBG1 ("0x");
            DBG_H (rec[i]);
	    DBG1 (" ");
        }
	//DBG1 (rec);
	DBG1 ("\n");
	espconn_sent(conn, rec, len);
	
	//espconn_sent(conn, data, len); //тест ЭХО

}

static void ICACHE_FLASH_ATTR serverReconCb(void *arg, sint8 err) {
	struct espconn *conn=arg;
	if (conn==NULL) return;
	os_printf("Recon callback on conn %p\n", conn);
	//Yeah... No idea what to do here. ToDo: figure something out.
}

static void ICACHE_FLASH_ATTR serverConnectCb(void *arg) {   
       
	struct espconn *conn = arg;
	os_printf("Connect callback on conn %p\n", conn);
	
	connData.conn=conn;
	connData.txbufferlen = 0;
	connData.readytosend = true;

	espconn_regist_recvcb(conn, serverRecvCb);
	espconn_regist_reconcb(conn, serverReconCb);
	espconn_regist_disconcb(conn, serverDisconCb);
	espconn_regist_sentcb(conn, serverSentCb);

}   