<html>
<head><title>Modbus TCP 2 Modbus RTU converter</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="wifi/style.css">
<script type="text/javascript" src="wifi/140medley.min.js"></script>
<script type="text/javascript">

var xhr=j();
var currAp="%currSsid%";

function getModbus() {
	xhr.open("GET", "getModbus.cgi");
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status>=200 && xhr.status<300) {
			//alert(xhr.responseText);
			var data=JSON.parse(xhr.responseText);
			document.getElementById('port').value = data.port;
			document.getElementById('baud').value = data.baud;
			document.getElementById('parity').value = data.parity;
			document.getElementById('data').value = data.data;
			document.getElementById('stop').value = data.stop;
			document.getElementById('CRLF').value = data.CRLF;
		}

	}
	xhr.send();
}


window.onload=function(e) {
	getModbus();
};
</script>

<style>
   
 </style>


</head>
<body leftmargin="0" topmargin="0">



<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" height="100" bgcolor="#005155">
      <p style="margin-left:8px;margin-top:10px;"><a href="http://www.wiregeo.com"><img src="WireGeoS.gif"></a><font size="5" style="vertical-align:0px; margin-left:30px; color:#FFFFFF"><b>WiFi RS-485 Router Setup</b></font></p>
	  <hr size="1"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><b><a class="aa current" href="#" target="a">RS-485 &amp; Port Settings</a> <a class="aa" href="/wifi" target="a">Wi-Fi Connection</a> <a class="aa" href="led.tpl" target="a">Diagnostics</a> </b></td>
        <td><p align="right" style="margin-right:10; color:#FFFFFF">Version: 1.0</p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign="top">
    
    
      <div style="margin-left:20;">&nbsp;
      
      <p><b>Overview</b></p>
      <p>WiFi RS-485 Router connects to your Wi-Fi network, accepts incoming TCP requests on given TCP Port and passes received data to its RS-485 port. All data received from RS-485 port is then sent back to remote TCP client as an answer. </p>
      
      <p>
      <form name="Modbusform" action="setModbus.cgi" method="post">
        <table border="0">
          <tr>
            <td colspan="3"><b>Network Port Settings</b></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="50">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>TCP Port:</td>
            <td width="50">&nbsp;</td>
            <td><input type="text"  id="port" name="port" val="%port%"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="50">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><b>RS-485 Port Settings</b></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="50">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Baud:</td>
            <td width="50">&nbsp;</td>
            <td><select id="baud" name="baud">
              <option value="300">300</option>
              <option value="600">600</option>
              <option value="1200">1200</option>
              <option value="2400">2400</option>
              <option value="4800">4800</option>
              <option value="9600">9600</option>
              <option value="19200">19200</option>
              <option value="38400">38400</option>
              <option value="57600">57600</option>
              <option value="74880">74880</option>
              <option value="115200">115200</option>
              <option value="230400">230400</option>
              <option value="460800">460800</option>
              <option value="921600">921600</option>
              <option value="1843200">1843200</option>
              <option value="3686400">3686400</option>
            </select></td>
          </tr>
          <tr>
            <td>Parity:</td>
            <td width="50">&nbsp;</td>
            <td><select id="parity" name="parity">
              <option value="2">None</option>
              <option value="1">ODD</option>
              <option value="0">EVEN</option>
            </select></td>
          </tr>
          <tr>
            <td>Bit Data:</td>
            <td width="50">&nbsp;</td>
            <td><select id="data" name="data">
              <option value="0">5</option>
              <option value="1">6</option>
              <option value="2">7</option>
              <option value="3">8</option>
            </select></td>
          </tr>
          <tr>
            <td>Stop Bit:</td>
            <td width="50">&nbsp;</td>
            <td><select id="stop" name="stop">
              <option value="1">1</option>
              <option value="2">1/2</option>
              <option value="3">2</option>
            </select></td>
          </tr>
          <tr>
            <td>CRLF:</td>
            <td width="50">&nbsp;</td>
            <td><select id="CRLF" name="CRLF">
              <option value="0">off</option>
              <option value="1">on</option>
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="50">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="50">&nbsp;</td>
            <td><input type="submit" name="save" value="Save"></td>
          </tr>
        </table>
      
        
      </form>
      </p>
    </div>
    
    </td>
  </tr>
  <tr>
    <td height="30"  bgcolor="#005155"><p align="center" style="color:#FFFFFF">© 2016 WireGeo</p></td>
  </tr>
</table>




</body></html>
