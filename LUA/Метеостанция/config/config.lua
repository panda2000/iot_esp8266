gpio.mode(esp_gpio[1],  gpio.INPUT)
local d1=gpio.read(esp_gpio[1])
gpio.mode(esp_gpio[1],  gpio.OUTPUT)
print("GPIO1",d1)

ssid=nil
pass=nil
dhcp=nil
ip_st=nil 
mask_st=nil 
gateway_st=nil
if (file.open("esp.conf","r")~= nil) then
 ssid=file.readline()
 if ssid~=nil then ssid=string.sub (ssid,1,-2) end
 pass=file.readline()
 if pass~=nil then pass=string.sub (pass,1,-2) end
 dhcp=file.readline()
 if dhcp~=nil then dhcp=string.sub (dhcp,1,-2) end
 ip_st=file.readline()
 if ip_st~=nil then ip_st=string.sub (ip_st,1,-2) end 
 mask_st=file.readline()
 if mask_st~=nil then mask_st=string.sub (mask_st,1,-2) end
 gateway_st= file.readline()
 if gateway_st~=nil then gateway_st = string.sub (gateway_st,1,-2) end 
 file.close()
else
 print ("File esp.conf not found ")
end

if (ssid == nil or pass == nil) then
 request="CONFIG MODE"
 print ("Wrong esp.conf parameters!!! ")
 dofile("web_ui.lua")
else
 dofile ("wifi.lua")
end

ssid = nil
pass = nil
collectgarbage()
