/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Jeroen Domburg <jeroen@spritesmods.com> wrote this file. As long as you retain 
 * this notice you can do whatever you want with this stuff. If we meet some day, 
 * and you think this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */

/*
This is example code for the esphttpd library. It's a small-ish demo showing off 
the server, including WiFi connection management capabilities, some IO and
some pictures of cats.
*/

#include <esp8266.h>
#include "osapi.h"

#include "io.h"
#include "httpd.h"
#include "httpdespfs.h"
#include "cgi.h"
#include "cgiwifi.h"
#include "cgiflash.h"
#include "auth.h"
#include "espfs.h"
#include "captdns.h"
#include "webpages-espfs.h"

#include <flash_rw.h>
#include "driver/uart.h"
#include "porttcp.h"

#define DEVICE_SSID "WireGEO-TCP2RTU"
#define DEVICE_PASS "12345678"
#define CRLF_EN 0

//The example can print out the heap use every 3 seconds. You can use this to catch memory leaks.
//#define SHOW_HEAP_USE
// Test r/w data from the flash memory
//#define TEST_FLASH
//#define TEST_IP
//#define SET_SSID


//------------------------ Gateway ------------------------------------------
typedef enum{
    GW_ENOERR,                  /*!< no error. */
    GW_ENOREG,                  /*!< illegal register address. */
    GW_EINVAL,                  /*!< illegal argument. */
    GW_EPORTERR,                /*!< porting layer error. */
    GW_ENORES,                  /*!< insufficient resources. */
    GW_EIO,                     /*!< I/O error. */
    GW_EILLSTATE,               /*!< protocol stack in illegal state. */
    GW_ETIMEDOUT                /*!< timeout error occurred. */
} GW_ErrorCode;


/* ----------------------- Modbus includes ----------------------------------*/
//#include "mb.h"
//#include "mbport.h"

int CRLF = 0;

/* ----------------------- Defines ------------------------------------------*/
#define REG_INPUT_START 0
#define REG_INPUT_NREGS 4
#define MB_PORT 502 
#define USE_MODBUS_TCP
#define USE_MODBUS_RTU
#define UART_TIMEOUT	10	// sec

/* ----------------------- Static variables ---------------------------------*/
//typedef unsigned short USHORT;
//typedef unsigned char UCHAR;

//static USHORT   usRegInputStart = REG_INPUT_START;
//static USHORT   usRegInputBuf[REG_INPUT_NREGS];

//----------------------------------------------------------------------------

//eMBErrorCode  ICACHE_FLASH_ATTR uart_rtu_rx (uint8 func, UCHAR * slave,  USHORT addr,  USHORT n, UCHAR * pucRegBuffer, uint8 * send_buff);
//void ICACHE_FLASH_ATTR modbus_rtu_tx (uint8 func, UCHAR * slave,  USHORT addr,  USHORT count, UCHAR * pucRegBuffer, uint8 * buff_data);

//Function that tells the authentication system what users/passwords live on the system.
//This is disabled in the default build; if you want to try it, enable the authBasic line in
//the builtInUrls below.
int ICACHE_FLASH_ATTR  myPassFn(HttpdConnData *connData, int no, char *user, int userLen, char *pass, int passLen) {
	if (no==0) {
		os_strcpy(user, "admin");
		os_strcpy(pass, "s3cr3t");
		return 1;
//Add more users this way. Check against incrementing no for each user added.
//	} else if (no==1) {
//		os_strcpy(user, "user1");
//		os_strcpy(pass, "something");
//		return 1;
	}
	return 0;
}

#ifdef ESPFS_POS
CgiUploadFlashDef uploadParams={
	.type=CGIFLASH_TYPE_ESPFS,
	.fw1Pos=ESPFS_POS,
	.fw2Pos=0,
	.fwSize=ESPFS_SIZE,
};
#define INCLUDE_FLASH_FNS
#endif
#ifdef OTA_FLASH_SIZE_K
CgiUploadFlashDef uploadParams={
	.type=CGIFLASH_TYPE_FW,
	.fw1Pos=0x1000,
	.fw2Pos=((OTA_FLASH_SIZE_K*1024)/2)+0x1000,
	.fwSize=((OTA_FLASH_SIZE_K*1024)/2)-0x1000,
};
#define INCLUDE_FLASH_FNS
#endif

/*
This is the main url->function dispatching data struct.
In short, it's a struct with various URLs plus their handlers. The handlers can
be 'standard' CGI functions you wrote, or 'special' CGIs requiring an argument.
They can also be auth-functions. An asterisk will match any url starting with
everything before the asterisks; "*" matches everything. The list will be
handled top-down, so make sure to put more specific rules above the more
general ones. Authorization things (like authBasic) act as a 'barrier' and
should be placed above the URLs they protect.
*/
HttpdBuiltInUrl builtInUrls[]={
	{"*", cgiRedirectApClientToHostname, "esp8266.nonet"},
	{"/", cgiRedirect, "/index.tpl"},
	//{"/flash.bin", cgiReadFlash, NULL},
	{"/led.tpl", cgiEspFsTemplate, tplLed},
	{"/index.tpl", cgiEspFsTemplate, tplCounter},
	{"/led.cgi", cgiLed, NULL},
	{"/getModbus.cgi", cgiGetModbusParam, NULL},
	{"/setModbus.cgi", cgiSetModbusParam, NULL},
	//{"/flash/download", cgiReadFlash, NULL},
#ifdef INCLUDE_FLASH_FNS
	{"/flash/next", cgiGetFirmwareNext, &uploadParams},
	{"/flash/upload", cgiUploadFirmware, &uploadParams},
#endif
	{"/flash/reboot", cgiRebootFirmware, NULL},

	//Routines to make the /wifi URL and everything beneath it work.

//Enable the line below to protect the WiFi configuration with an username/password combo.
//	{"/wifi/*", authBasic, myPassFn},

	{"/wifi", cgiRedirect, "/wifi/wifi.tpl"},
	{"/wifi/", cgiRedirect, "/wifi/wifi.tpl"},
	{"/wifi/wifiscan.cgi", cgiWiFiScan, NULL},
	{"/wifi/cgigetip.cgi", cgiGetIP, NULL},
	{"/wifi/wifi.tpl", cgiEspFsTemplate, tplWlan},
	{"/wifi/connect.cgi", cgiWiFiConnect, NULL},
	{"/wifi/connstatus.cgi", cgiWiFiConnStatus, NULL},
	{"/wifi/setmode.cgi", cgiWiFiSetMode, NULL},

	{"*", cgiEspFsHook, NULL}, //Catch-all cgi function for the filesystem
	{NULL, NULL, NULL}
};


#ifdef SHOW_HEAP_USE
static ETSTimer prHeapTimer;

static void ICACHE_FLASH_ATTR prHeapTimerCb(void *arg) {
	os_printf("Heap: %ld\n", (unsigned long)system_get_free_heap_size());
}
#endif

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static os_timer_t logic_timer;

uint32 flash_adr = 0x3C000;	// для теста

//Timer function
static void ICACHE_FLASH_ATTR
logic_timerfunc(os_event_t *events)
{
#ifdef TEST_IP
	struct ip_info info;
	wifi_get_ip_info(SOFTAP_IF, &info);
	
	DBG1 ("\nIP AP: ");
	DBG16 ((info.ip.addr>>0)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>8)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>16)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>24)&0xff); DBG1 (".");
	DBG1 ("\n");
	
	wifi_get_ip_info(STATION_IF, &info);
	
	//os_printf("\nSet static IP = %d\n",wifi_station_connect());
	DBG1 ("\nIP : ");
	DBG16 ((info.ip.addr>>0)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>8)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>16)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>24)&0xff); DBG1 (".");
	DBG1 ("\n");
	
#endif
#ifdef TEST_FLASH
	uint32 value;
	uint8 *data = (uint8 *)&value;
//	uint8 *adr = (uint8 *)&flash_adr;
//	struct esp_platform_sec_flag_param ;
/*
	//uint16 adr16 = 0x3C;
	if (flash_adr < 0x3C004) 
	    value = 0x123456;
	else 
	    value = 0xFFCCDD;
*/	
	if (flash_adr < 0x3C008)
	{
	    //adr16 = (uint16) (flash_adr/SPI_FLASH_SEC_SIZE);
	    //spi_flash_erase_sector (adr16);
	    //spi_flash_write (flash_adr, &flash_adr, 1);	
//	    user_esp_platform_save_param(&value, sizeof(value));
	
// 	    spi_flash_read(flash_adr, (uint32 *)data, 4);
	    user_esp_platform_load_param(&value, sizeof(value));
/*
 	    os_printf("adr:%02x%02x%02x%02x sec:%02x%02x%02x%02x\r\n", 
			adr[3], adr[2], adr[1],adr[0],
			data[3], data[2], data[1],data[0]);
*/
	    os_printf("0x3C flag:%02x data0:%02x%02x%02x%02x\r\n", 
			temp.flag, data[3], data[2], data[1], data[0]);

	    flash_adr += 4;
	    
	}
#endif
}


void ICACHE_FLASH_ATTR Gateway_config (void){
    readytxsend = false; // готовность данных полученных от ведомого устройства, к отправке
    struct esp_parameters info;
    user_esp_platform_load_param(&info, sizeof(info));

#ifdef USE_MODBUS_RTU
    system_set_os_print(0); //0/1 = debug off/on
    // Initialize UART1 to use as debug
    uart_init(115200,115200);
    
    // Initialize UART0 to use as Modbus RTU
    print_ModbusRTU_param(&info);
    UART_SetBaudrate	(UART0, info.Baud);  
    UART_SetParity	(UART0, info.parity);
    UART_SetWordLength	(UART0, info.data);
    UART_SetStopBits	(UART0, info.stop);
	
    Slave_adr = info.adr;
#endif
	
//#ifdef USE_MODBUS_TCP	
//    eMBErrorCode    eStatus;

    // CRLF для пакетной передачи
    if (info.CRLF == 0)
	CRLF == 0;
    else
	CRLF == 1;

    // Инициализируем Modbus TCP
    if (info.port == 0)
	//eStatus = eMBTCPInit
	xMBTCPPortInit( MB_PORT );
    else
	//eStatus = eMBTCPInit
	xMBTCPPortInit( info.port );
    
    print_ModbusTCP_param(&info, MB_PORT);
    
    // Enable the Modbus Protocol Stack. //
    //eStatus = eMBEnable(  );
    //os_printf("Stack Enabled = %d\n", eStatus);
//#endif	
}

//------------------------------------------------------------------------------------------

void ICACHE_FLASH_ATTR static_ip (void){	// Устанавливаем статические параметры сети
	
    struct ip_info info;
    wifi_set_sleep_type(NONE_SLEEP_T);

    wifi_softap_dhcps_stop();
	//os_printf("\nWi-Fi Mode: %d\n" , wifi_get_opmode());
    if (wifi_get_opmode() != 0x03) {	// ! soft-AP mode
	wifi_station_disconnect();
	//os_printf("\nSet static IP\n" );
	wifi_station_dhcpc_stop();	

	struct esp_parameters net_par;
	user_esp_platform_load_param(&net_par, sizeof(net_par));

	info.ip.addr = net_par.ip;
	info.netmask.addr = net_par.mask;
	info.gw.addr = net_par.gw;

	wifi_set_ip_info(STATION_IF, &info);
	wifi_set_ip_info(SOFTAP_IF, &info);
	
	//os_printf("\nSet static IP = %d\n",wifi_station_connect());
	DBG1 ("\nIP : ");
	DBG16 ((info.ip.addr>>0)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>8)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>16)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>24)&0xff); DBG1 (".");
	DBG1 ("\n");
	
    }
    else{
	DBG1("\nsoft-AP mode\n" );
#ifdef SET_SSID
	struct softap_config config;
	char ssid[32] = DEVICE_SSID;
	char pass[32] = DEVICE_PASS;
	wifi_softap_get_config(&config); // Get config first.
	os_printf ("Old SSID : %s\n", config.ssid);
	os_memset(config.ssid, 0, 32);
	os_memset(config.password, 0, 32);
	os_memcpy(config.ssid, ssid, 32);
	os_memcpy(config.password, pass, 32);
	config.authmode = AUTH_WPA_WPA2_PSK;
	config.ssid_len = 0;// or its actual length
	config.max_connection = 4; // how many stations can connect to ESP8266 softAP at most.

	if (wifi_softap_set_config(&config))// Set ESP8266 softap config .
	    os_printf ("Start SSID : %s\n", config.ssid);
	else
	    os_printf ("Start SSID err: %s\n", config.ssid);
#endif	
	wifi_softap_dhcps_start();
	wifi_station_dhcpc_start();
	
	wifi_get_ip_info(SOFTAP_IF, &info);
	
	//os_printf("\nSet static IP = %d\n",wifi_station_connect());
	DBG1 ("\nIP : ");
	DBG16 ((info.ip.addr>>0)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>8)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>16)&0xff); DBG1 (".");
	DBG16 ((info.ip.addr>>24)&0xff); DBG1 (".");
	DBG1 ("\n");

    }
   // DBG16 (wifi_get_opmode());
}

//Main routine. Initialize stdout, the I/O, filesystem and the webserver and we're done.
void ICACHE_FLASH_ATTR user_init(void) {
//	struct ip_info info;

	// Initialize UART0 to use as debug
	//system_set_os_print(1);
	//uart_div_modify(0, UART_CLK_FREQ / 115200);
	
//	ioInit();
	Gateway_config ();
	captdnsInit();
	static_ip();


	// 0x40200000 is the base address for spi flash memory mapping, ESPFS_POS is the position
	// where image is written in flash that is defined in Makefile.
#ifdef ESPFS_POS
	espFsInit((void*)(0x40200000 + ESPFS_POS));
	//os_printf("\nESPFS_POS is defined = %06x ESPFS_SIZE =  %06x\n",ESPFS_POS, ESPFS_SIZE);
#else
	espFsInit((void*)(webpages_espfs_start));
#endif
	httpdInit(builtInUrls, 80);
#ifdef SHOW_HEAP_USE
	os_timer_disarm(&prHeapTimer);
	os_timer_setfn(&prHeapTimer, prHeapTimerCb, NULL);
	os_timer_arm(&prHeapTimer, 3000, 1);
#endif
	
	DBG1 ("\nReady\n");

    os_timer_disarm(&logic_timer);
    os_timer_setfn(&logic_timer, (os_timer_func_t *)logic_timerfunc, NULL);
    os_timer_arm(&logic_timer, 1500, 1);
    
    //uint8 buf_data[8+4];
    //UCHAR * pucRegBuffer;
    //modbus_rtu_tx (0x03, "1",  0x00, 0x04, pucRegBuffer, & buf_data[0]);
    //uart_rtu_rx (0x03, "1",  0x00, 0x04, pucRegBuffer, & buf_data[0]);
	
}

void user_rf_pre_init() {
	//Not needed, but some SDK versions want this defined.
}

//-----------------------------------------------------------------------------------------------------
// Gateway functions
//-----------------------------------------------------------------------------------------------------
GW_ErrorCode  ICACHE_FLASH_ATTR uart_rtu_rx_ (uint8 * data, unsigned short * len){
	
    *len = 0;
//	uint8 * data1= data;
    //USHORT CRC = 0;
    uint8 l = 0;
	
    int i = UART_TIMEOUT *40;	// 
    while (!readytxsend && i> 0){	// ждем ответа slave устройства
	os_delay_us(25*1000);
	i--;
    }
    if (!readytxsend && i == 0){
	DBG1("TIMEOUT\n");
	return GW_ETIMEDOUT;	// отправляем ошибку таймаута
    }

    // ответ получен приступаем к проверке    
//    DBG1("\nTime = ");
//    DBG16(UART_TIMEOUT *40 - i);
//    DBG1("\nUARTbuf=");
    i = UART_TIMEOUT;
    while (readytxsend || i > 0){
	if (readytxsend){
	    for (l = 0; l < uartbuffer_len; l++){
		*data = uartbuffer[l];
		data++;
	    }
	    readytxsend = 0;
	    *len += uartbuffer_len;
	    i=UART_TIMEOUT;
	}
	os_delay_us(5*1000);	// ожидание продолжения пакета
	i--;			// 5 попыток (всего 25 мс)
    }

    data = 0;
     
    return GW_ENOERR;	// No error
}

//--------------------------------------------------------------------------------------
void ICACHE_FLASH_ATTR uart_rtu_tx_ (char *data, unsigned short len){
    
//    DBG1("\nUART send len=");
//    DBG16(len);
//    DBG1("\n");
/*    DBG1("\nUART send buf=");
    int k=0;
    for (k = 0; k < len; k++){
	DBG_H (data[k]);
	DBG1(" ");
    }
    DBG1("\n");
*/	
    uart0_tx_buffer(data, len);
}


GW_ErrorCode  ICACHE_FLASH_ATTR Poll( char *data_send,  char *data_receive, unsigned short * len)
{
    DBG1 ("TCP2RTUPoll\n");
    readytxsend = false;
    GW_ErrorCode result;
    if (CRLF && CRLF_EN){	// Использовать разделители пакета
	char send_buf [MAX_TCP_BUFF];
	char receive_buf [MAX_TCP_BUFF];
	
	int send_len = *len;
	int recive_len =0;
	int n=0;
	char * char_index = data_send;
	while (n < send_len )
	{
	    int l=0;
	    // Парсим пакеты до <CR><LF>
	    for (; *char_index != 0x0A && n+l < send_len ; char_index++){
		send_buf [l] = *char_index;
		l++;
	    }
	    if (n+l < send_len){
		send_buf [l] = *char_index;
		char_index++;
		l++;
	    }
	    uart_rtu_tx_ (&send_buf[0], l);
	    n+=l;
	    
	    result = uart_rtu_rx_ (&receive_buf[recive_len], (unsigned short *) &l);
	    recive_len += l;
	    receive_buf[recive_len]=0;
	    //DBG1 ("\n-Recive Len = ");
	    //DBG16 (recive_len);
	    //DBG1 ("\n-Recive data = ");
	    //DBG1 (&receive_buf[0]);
	    //DBG1 ("\n");
	    //DBG1 ("\n-n = ");
	    //DBG16 (n);
	    //DBG1 ("\n");
	    //os_delay_us(10*1000);
	    
	}
	
	for (n=0; n<recive_len; n++){
		*data_receive = receive_buf[n];
		data_receive++;
	}
	*len = recive_len;
    }
    else{
	uart_rtu_tx_ (data_send, *len);
	result = uart_rtu_rx_ (data_receive, len);
    }
    return result;

}