sda_pin=13
scl_pin=14
esp_pin={["SHT"]={0}}
-- ESP-01 GPIO Mapping
esp_gpio={3,10,4,9,2,1,"","","",11,12,"", 6, 7, 5, 8, 0,17}
t = 0;

esp_data={"","","","","","","","","","","","","","","","",""}
request=""
esp_id = "ESP-01"  
id=0
dev_addr=0x70 --0xE1  

i2c.setup(id, esp_gpio[sda_pin+1], esp_gpio[scl_pin+1] ,i2c.SLOW)

uart.on("data", 9, 
  function(data)
  esp_data[2] = string.byte(string.sub (data, 3, 4)) * 256 + string.byte(string.sub (data, 4, 5))
  print (string.byte(data))
  --print ("3=",string.sub (data, 3, 4), "4=",string.sub (data, 4, 5))
  print (string.byte(string.sub (data, 3, 4)), string.byte(string.sub (data, 4, 5)) )
  --uart.on("data")
end, 0)


function get_data()
      uart.write (0, string.char(0xFF,0x01,0x86,0x00,0x00,0x00,0x00,0x00,0x79))
      i2c.start(id)
      i2c.address(id, dev_addr ,i2c.TRANSMITTER)
      i2c.write(id,0x09)
      i2c.write(id,0x00)
      i2c.write(id,0x00)

      i2c.stop(id)
      i2c.start(id)
      i2c.address(id, dev_addr,i2c.RECEIVER)
      c =i2c.read(id,6)
      i2c.stop(id)

      co2 = string.byte(string.sub(c,1,1))
      reactivity = string.byte(string.sub(c,2,2))
      tvoc = string.byte(string.sub(c,3,3))
      D4=string.byte(string.sub(c,4,4))
      D5=string.byte(string.sub(c,5,5))
      D6=string.byte(string.sub(c,6,6))
--      print ("time = ", t*10, "min [",co2, reactivity, tvoc, D4, D5, D6,"]")
      --esp_data[0] = "6400" .. t*10
      esp_data[1] = co2
      --esp_data[2] = reactivity
      esp_data[3] = tvoc
      esp_data[4] = D4
      esp_data[5] = D5
      esp_data[6] = D6
      t=t+1
      
      co2 = (co2 - 13) * (1600.0 / 229) + 400   -- ppm: 400 .. 2000
      tvoc = (tvoc - 13) * (1000.0/229)   -- ppb: 0 .. 1000
      r = 10*(D4 + (256*D5) + (65536*D6))
      esp_data[7] = string.sub(co2,1,5)
      esp_data[8] = string.sub(tvoc,1,5)
      esp_data[9] = string.sub(r,1,5)
--      print ("CO2=",co2, "ppm VOC=", tvoc, "ppb react=",reactivity, "R=",r)
--      print () 
      mg811 = adc.read(0)/2.1
      esp_data[4] = mg811
--      print ("mg811 CO2=",mg811)
--      print () 

      dofile("request.lua")
      dofile("httpsender.lua")
      collectgarbage()
    return nil
end

function init_data()
      i2c.start(id)
      i2c.address(id, dev_addr ,i2c.TRANSMITTER)
      i2c.write(id,0x08)
      i2c.write(id,0x03)    --851 ppm
      i2c.write(id,0x53)
      i2c.stop(id)
      collectgarbage()
    return nil
end

--init.lua
--print("Setting up WIFI...")
uart.setup( 0, 9600, 8, 0, 1, 0 )
wifi.setmode(wifi.STATION)
--modify according your wireless router settings
wifi.sta.config("IT-home","fhntv2011")
wifi.sta.connect()
tmr.alarm(1, 1000, 1, function() 
if wifi.sta.getip()== nil then 
--print("IP unavaiable, Waiting...") 
else 
tmr.stop(1)
--print("Config done, IP is "..wifi.sta.getip())
--init_data()
get_data()
tmr.alarm(1, 20000, 1, function() get_data() end) --600
end 
end)
