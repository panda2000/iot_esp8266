#ifndef _PUBLISH_WIREGEO_H_
#define _PUBLISH_WIREGEO_H_
#include "os_type.h"
#include "osapi.h"
#include "md5.h"
#include "publish.h"

void publish_wiregeo_connect_cb(void *arg);
static void recv_wiregeo_cb(void *arg, char *data, unsigned short len);

#endif