<html>
<head><title>Modbus TCP 2 Modbus RTU converter</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="wifi/140medley.min.js"></script>
<script type="text/javascript">

var xhr=j();
var currAp="%currSsid%";

function getModbus() {
	xhr.open("GET", "getModbus.cgi");
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status>=200 && xhr.status<300) {
			//alert(xhr.responseText);
			var data=JSON.parse(xhr.responseText);
			document.getElementById('port').value = data.port;
			document.getElementById('baud').value = data.baud;
			document.getElementById('parity').value = data.parity;
			document.getElementById('data').value = data.data;
			document.getElementById('stop').value = data.stop;
		}

	}
	xhr.send();
}


window.onload=function(e) {
	getModbus();
};
</script>
</head>
<body>
<div id="main">
<h1>Modbus TCP - Modbus RTU шлюз</h1>
<p>
Данное устройство выполняет преобразование Modbus TCP пакетов в Modbus RTU.
<ul>
<li>Для работы с устройством подсоедините его к <a href="/wifi"> вашей Wi-Fi сети.</a></li>
<li>Также вы можете проверитьработоспособность <a href="led.tpl">индикатора</a>.</li>
</ul>
</p>

<p>
<form name="Modbusform" action="setModbus.cgi" method="post">
Настройте параметры Modbus TCP<br>
Порт: <input type="text"  id="port" name="port" val="%port%"> <br />
<br>
<!--Настройте параметры ведомого Modbus устройства<br>
Slave: <input type="text" id="slave" name="slave" val="%slave%"> <br />
<br>-->
Настройте параметры RS-485<br>
Baud: <select id="baud" name="baud">
  <option value="300">300</option>
  <option value="600">600</option>
  <option value="1200">1200</option>
  <option value="2400">2400</option>
  <option value="4800">4800</option>
  <option value="9600">9600</option>
  <option value="19200">19200</option>
  <option value="38400">38400</option>
  <option value="57600">57600</option>
  <option value="74880">74880</option>
  <option value="115200">115200</option>
  <option value="230400">230400</option>
  <option value="460800">460800</option>
  <option value="921600">921600</option>
  <option value="1843200">1843200</option>
  <option value="3686400">3686400</option>  
</select> <br />

Parity: <select id="parity" name="parity">
  <option value="2">None</option>
  <option value="1">ODD</option>
  <option value="0">EVEN</option>
</select> <br />

Bit data: <select id="data" name="data">
  <option value="0">5</option>
  <option value="1">6</option>
  <option value="2">7</option>
  <option value="3">8</option> 
</select> <br />
    
Stop bit: <select id="stop" name="stop">
  <option value="1">1</option>
  <option value="2">1/2</option>
  <option value="3">2</option>
</select> <br />
<br>
<input type="submit" name="save" value="Сохранить">
</p>
</form>
</div>
</body></html>
