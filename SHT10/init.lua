-- 10 ms
--sck 0 = 80 us 1 = 80 us
--data 0 = 400 us 1= 800 us
sda_pin=13
scl_pin=14
esp_pin={["SHT"]={0}}
-- ESP-01 GPIO Mapping
esp_gpio={3,10,4,9,2,1,"","","",11,12,"", 6, 7, 5, 8, 0,17}

local sda = 4
local scl = 3
 
local function sd(val)
--print("sd()")
if (val==0 or val==nil) then
gpio.write(sda, gpio.LOW)
gpio.mode(sda, gpio.OUTPUT)
else
gpio.mode(sda, gpio.INPUT)
gpio.write(sda, gpio.HIGH)
end end
 
local function sc(val)
--print("sc()")
if (val==0 or val==nil) then
gpio.write(scl, gpio.LOW)
else
gpio.write(scl, gpio.HIGH)
end end
 
local function dr()
--print("dr()")
 gpio.mode(sda, gpio.INPUT)
 return gpio.read(sda)
end
 
local function wait()
 for i = 1, 100 do
  tmr.delay(10000)
  if dr() == gpio.LOW then
   return true
  end
 end
 return false
end
 
local function read_byte()
 local val = 0
 for i = 0, 7 do
  sc(1)
  val = val * 2 + dr()
  sc(0)
 end
 return val
end
 
local function write_byte(val)
 for i = 0, 7 do
  if bit.band(val, 2 ^ (7-i)) == 0 then
   sd(0)
  else
   sd(1)
  end
  sc(1); sc(0)
 end
end
 
local function read_cmd(cmd)
 print("read_cmd(",cmd,")")
 sd(1) sc(1) sd(0) sc(0) sc(1) sd(1) sc(0)
 print("write_byte(",cmd,")")
 write_byte(cmd)
 sc(1); sc(0)
 if not wait() then
  return nil
 end
 sd(1)
 local val = read_byte() 
 sd(1) sd(0) sc(1) sc(0) sd(1)
 print("val = ",val)
 print("val * 256 =",val*256)
 val = val * 256 + read_byte()
 print("val = ",val)
 sd(1) sc(1) sc(0)
 return val
end
 
function init(d, l)
 print("init(d, l)")
 if d ~= nil then
  sda = d
 end
 if l ~= nil then
  scl = l
 end
 gpio.mode(sda, gpio.INPUT)
 gpio.mode(scl, gpio.OUTPUT)
 print("SHT1x init done")
end
 
function get_raw_temperature()
 print("get_raw_temperature()")
 return read_cmd(3)
end
 
function get_raw_humidity()
 print("get_raw_humidity()")
 return read_cmd(5)
end

init(esp_gpio[sda_pin+1], esp_gpio[scl_pin+1])
print("START")
 hum = get_raw_humidity()
 print ("RAW hum",hum)
 temp = get_raw_temperature()
 print ("RAW temp",temp)
