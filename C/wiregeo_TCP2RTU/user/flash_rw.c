//Save and load data from flash. Function with protect data.


#include <esp8266.h>
#include <flash_rw.h>
#include "driver/uart.h"

/******************************************************************************
 * FunctionName : user_esp_platform_load_param
 * Description  : load parameter from flash, toggle use two sector by flag value.
 * Parameters   : param--the parame point which write the flash
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_esp_platform_load_param(void *param, uint16 len)
{
    struct esp_platform_sec_flag_param flag;

    spi_flash_read((ESP_PARAM_START_SEC + ESP_PARAM_FLAG) * SPI_FLASH_SEC_SIZE,
                   (uint32 *)&flag, sizeof(struct esp_platform_sec_flag_param));

    if (flag.flag == 0) {
        spi_flash_read((ESP_PARAM_START_SEC + ESP_PARAM_SAVE_0) * SPI_FLASH_SEC_SIZE,
                       (uint32 *)param, len);
    } else {
        spi_flash_read((ESP_PARAM_START_SEC + ESP_PARAM_SAVE_1) * SPI_FLASH_SEC_SIZE,
                       (uint32 *)param, len);
    }
}

/******************************************************************************
 * FunctionName : user_esp_platform_save_param
 * Description  : toggle save param to two sector by flag value,
 *              : protect write and erase data while power off.
 * Parameters   : param -- the parame point which write the flash
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_esp_platform_save_param(void *param, uint16 len)
{
    struct esp_platform_sec_flag_param flag;

    spi_flash_read((ESP_PARAM_START_SEC + ESP_PARAM_FLAG) * SPI_FLASH_SEC_SIZE,
                       (uint32 *)&flag, sizeof(struct esp_platform_sec_flag_param));

    if (flag.flag == 0) {
        spi_flash_erase_sector(ESP_PARAM_START_SEC + ESP_PARAM_SAVE_1);
        spi_flash_write((ESP_PARAM_START_SEC + ESP_PARAM_SAVE_1) * SPI_FLASH_SEC_SIZE,
                        (uint32 *)param, len);
        flag.flag = 1;
        spi_flash_erase_sector(ESP_PARAM_START_SEC + ESP_PARAM_FLAG);
        spi_flash_write((ESP_PARAM_START_SEC + ESP_PARAM_FLAG) * SPI_FLASH_SEC_SIZE,
                        (uint32 *)&flag, sizeof(struct esp_platform_sec_flag_param));
    } else {
        spi_flash_erase_sector(ESP_PARAM_START_SEC + ESP_PARAM_SAVE_0);
        spi_flash_write((ESP_PARAM_START_SEC + ESP_PARAM_SAVE_0) * SPI_FLASH_SEC_SIZE,
                        (uint32 *)param, len);
        flag.flag = 0;
        spi_flash_erase_sector(ESP_PARAM_START_SEC + ESP_PARAM_FLAG);
        spi_flash_write((ESP_PARAM_START_SEC + ESP_PARAM_FLAG) * SPI_FLASH_SEC_SIZE,
                        (uint32 *)&flag, sizeof(struct esp_platform_sec_flag_param));
    }
}

/******************************************************************************
 * DEBUG output parameters
*******************************************************************************/
void ICACHE_FLASH_ATTR 
print_ModbusRTU_param(struct esp_parameters *info)
{
    DBG1 ("\nModbus RTU ");
    DBG1 ("\nSlave: ");
    DBG16 (info->adr);
    DBG1 ("\n Baud: ");
    DBG32 (info->Baud);
    DBG1 ("\nParity: ");
    switch (info->parity){
	case 0x01 : {
	    DBG1 ("ODD");
	    break;
	}
	case 0x02 : {
	    DBG1 ("NONE");
	    break;
	}
	default : {	// 0x00
	    DBG1 ("EVEN");
	}
    }	
    DBG1 ("\nData bits: ");
    DBG16 (info->data+5);
    DBG1 ("\nStop bits: ");
    switch (info->stop){
	case 0x01: {
	    DBG1("1");
	    break;
	}
	case 0x02: {
	    DBG1("1,5");
	    break;
	}
	default:	// 0x02
	    DBG1("2");
    }
    DBG1 ("\n");
}

void ICACHE_FLASH_ATTR 
print_ModbusTCP_param(struct esp_parameters *info, uint16 def_port)
{
    DBG1 ("\nModbus TCP\n Port: ");
    if (info->port == 0){
	DBG16 ( def_port );
    }
    else{
	DBG16 ( info->port );    
    }
    DBG1 ("\n");
}

void ICACHE_FLASH_ATTR 
print_TCP_param(struct esp_parameters *info)
{
	
}