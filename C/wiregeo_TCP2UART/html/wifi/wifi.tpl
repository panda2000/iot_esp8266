<html><head><title>Подключение к WiFi </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="140medley.min.js"></script>
<script type="text/javascript">

var xhr=j();
var currAp="%currSsid%";

function createInputForAp(ap) {
	if (ap.essid=="" && ap.rssi==0) return;
	var div=document.createElement("div");
	div.id="apdiv";
	var rssi=document.createElement("div");
	var rssiVal=-Math.floor(ap.rssi/51)*32;
	rssi.className="icon";
	rssi.style.backgroundPosition="0px "+rssiVal+"px";
	var encrypt=document.createElement("div");
	var encVal="-64"; //assume wpa/wpa2
	if (ap.enc=="0") encVal="0"; //open
	if (ap.enc=="1") encVal="-32"; //wep
	encrypt.className="icon";
	encrypt.style.backgroundPosition="-32px "+encVal+"px";
	var input=document.createElement("input");
	input.type="radio";
	input.name="essid";
	input.value=ap.essid;
	if (currAp==ap.essid) input.checked="1";
	input.id="opt-"+ap.essid;
	var label=document.createElement("label");
	label.htmlFor="opt-"+ap.essid;
	label.textContent=ap.essid;
	div.appendChild(input);
	div.appendChild(rssi);
	div.appendChild(encrypt);
	div.appendChild(label);
	return div;
}

function getSelectedEssid() {
	var e=document.forms.wifiform.elements;
	for (var i=0; i<e.length; i++) {
		if (e[i].type=="radio" && e[i].checked) return e[i].value;
	}
	return currAp;
}


function scanAPs() {
	xhr.open("GET", "wifiscan.cgi");
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status>=200 && xhr.status<300) {
			var data=JSON.parse(xhr.responseText);
			currAp=getSelectedEssid();
			if (data.result.inProgress=="0" && data.result.APs.length>1) {
				$("#aps").innerHTML="";
				for (var i=0; i<data.result.APs.length; i++) {
					if (data.result.APs[i].essid=="" && data.result.APs[i].rssi==0) continue;
					$("#aps").appendChild(createInputForAp(data.result.APs[i]));
				}
				window.setTimeout(scanAPs, 20000);
			} else {
				window.setTimeout(scanAPs, 1000);
			}
		}
	}
	xhr.send();
}

function getIP() {
	xhr.open("GET", "cgigetip.cgi");
	xhr.onreadystatechange=function() {
		if (xhr.readyState==4 && xhr.status>=200 && xhr.status<300) {
			var data=JSON.parse(xhr.responseText);
			document.getElementById('ip').value = data.ip;
			document.getElementById('mask').value = data.mask;
			document.getElementById('gw').value = data.gw;
			document.getElementById('mac').innerHTML = data.mac;
			document.getElementById('macAP').innerHTML = data.macAP;
		}

	}
	xhr.send();
	window.setTimeout(scanAPs, 1000);
}


window.onload=function(e) {
	getIP();
};
</script>
</head>
<body leftmargin="0" topmargin="0">


<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" height="100" bgcolor="#005155">
      <p style="margin-left:8px;margin-top:10px;"><a href="http://www.wiregeo.com"><img src="/WireGeoS.gif"></a><font size="5" style="vertical-align:0px; margin-left:30px; color:#FFFFFF"><b>WiFi RS-485 Router Setup</b></font></p>
	  <hr size="1"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><b><a class="aa" href="/index.tpl" target="a">RS-485 &amp; Port Settings</a> <a class="aa current" href="#" target="a">Wi-Fi Connection</a> <a class="aa" href="/led.tpl" target="a">Diagnostics</a> </b></td>
        <td><p align="right" style="margin-right:10; color:#FFFFFF">Version: 1.0</p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign="top">
    
    
    <div style="margin-left:20;">&nbsp;
      
   	    <p><b>Network Status</b></p>
            
          <table border="0">
            <tr>
              <td width="250">MAC:</td>
              <td width="50">&nbsp;</td>
              <td><div id="mac">??:??:??:??:??:??</div></td>
            </tr>
            <tr>
              <td width="250">MAC-AP:</td>
              <td width="50">&nbsp;</td>
              <td><div id="macAP">??:??:??:??:??:??</div></td>
            </tr>
            <tr>
              <td width="250">Current WiFi mode:</td>
              <td width="50">&nbsp;</td>
              <td>%WiFiMode%</td>
            </tr>
          </table>
       
        <form name="wifiform" action="connect.cgi" method="post">
          <p>
          
          <p><b>Wi-Fi Setup</b></p>
          
          <p>To connect to a Wi-Fi network, please select one of the detected networks:</p>
                    
          <div id="aps">Scanning...</div>
          
          <p>&nbsp;</p>
          <table border="0">
            <tr>
              <td width="250">WiFi password, if applicable:</td>
              <td width="50">&nbsp;</td>
              <td><input type="text" name="passwd" val="%WiFiPasswd%"></td>
            </tr>
            <tr>
              <td width="250">IP: </td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="ip" name="ip" val="%ip%"></td>
            </tr>
            <tr>
              <td width="250">Mask: </td>
              <td>&nbsp;</td>
              <td><input type="text" id="mask" name="mask" val="%mask%"></td>
            </tr>
            <tr>
              <td width="250">Gateway:</td>
              <td>&nbsp;</td>
              <td><input type="text" id="gw" name="gw" val="%gw%"></td>
            </tr>
            <tr>
              <td width="250">&nbsp;</td>
              <td width="50">&nbsp;</td>
              <td><input type="submit" name="connect" value="Connect!"></td>
            </tr>
          </table>
          
          </p>
        </form>

    </div>
    
    </td>
  </tr>
  <tr>
    <td height="30"  bgcolor="#005155"><p align="center" style="color:#FFFFFF">© 2016 WireGeo</p></td>
  </tr>
</table>




</body>
</html>
