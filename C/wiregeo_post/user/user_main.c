#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
#include "packets.h"
#include "logic.h"
#include <mem.h>

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1

#include "espconn.h" 
LOCAL struct espconn *pCon = NULL;
LOCAL my_count = 0;
LOCAL your_value = 33;

 os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;


void interupt_gpio4() {
   ETS_GPIO_INTR_DISABLE(); // Disable gpio interrupts
   //wdt_feed();

   uint32 gpio_status;
   gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
   //clear interrupt status
   GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status);

   os_printf("%d: GPIO Interrupt! %d\r\n", my_count++, gpio_status);
   os_delay_us(50000);
   GPIO_OUTPUT_SET(2, 0); 

   ETS_GPIO_INTR_ENABLE(); // Enable gpio interrupts
}

static void ICACHE_FLASH_ATTR networkRecvCb(void *arg, char *data, unsigned short len) {
 
  struct espconn *conn=(struct espconn *)arg;
  //os_printf("RECIVE:\r\n%s\r\nLEN: %d\r\n", data, len);
  data = strstr(data, "id=");
  os_printf("RECIVE DATA: %s\r\n",data);
  if (data != NULL){
    int x = strcspn(data,"&");
    char id[32];
    char data_[512];
    char dt[10];
    strncpy(id,data,x);
    id[x+1]='\0';
    os_printf("id: %s  %d\r\n", id, x);
	
    data = strstr(data, "data=");
    x = strcspn(data,"&");
    strncpy(data_,data,x);
    data_[x+1]='\0';
    channels_parse (data_);
	
    data = strstr(data, "dt=");
    os_printf("data: %s\r\n", data);
  }
}

static void ICACHE_FLASH_ATTR publish_wiregeo_connect_cb(void *arg)
{
  //INFO("========>publish_wiregeo_connect_cb\r\n");
  my_count++;

  struct espconn *pespconn = (struct espconn *)arg;

   char transmission[1024];
 
  char *header = "POST /sync_p.php HTTP/1.1\r\n"
                 "Host:  cloud.wiregeo.com\r\n"
                 "Accept: */*\r\n"
                 "User-Agent: esp8266\r\n"
		 "Connection: close\r\n"
                 "Content-Type: application/x-www-form-urlencoded\r\n"
                 "Content-Length: ";
  
  char json_data [1024];
  strcpy(json_data,"\0");
  char *json = json_data;
  strcat(json,"id=");
	os_printf("channels_generate1: %s\r\n", json);
  strcat(json,ESP_ID);
	os_printf("channels_generate2: %s\r\n", json);
  strcat(json,"&data=");
  channels_generate (json);
  strcat(json,"\r\n");
  os_printf("channels_generate3: %s\r\n", json);
	
  int head_len = strlen(header);
  int json_len = strlen(json);
 
  char json_len_str[10];
  os_sprintf(json_len_str,"%d",json_len);
  int json_len_str_len = strlen(json_len_str);
 
  strcpy(transmission,header);
  strcpy(transmission+head_len,json_len_str);
  strcpy(transmission+head_len+json_len_str_len,"\r\n\r\n");
  strcpy(transmission+head_len+json_len_str_len+4,json);
 
  //os_printf("%s\r\n", transmission);
  os_printf("SEND: %s\r\n", json);
  
  espconn_regist_recvcb(pespconn, networkRecvCb);
  
  sint8 d = espconn_sent(pespconn,transmission,strlen(transmission));
  
  os_printf("ERR: %d\r\n", d);
  
}

void pCon_connect()
{
    os_printf("Try espconn_connect\r\n");
    int ret = 0;	
    ret = espconn_connect(pCon);
	
    if(ret == 0) 
	//INFO("espconn_connect OK!\r\n");
	os_printf("espconn_connect OK!\r\n");
    else
    {
	os_printf("espconn_connect FAILED (%d)!\r\n",ret);
	//clean up allocated memory
	if (pCon != NULL)
	{
	    if(pCon->proto.tcp)
                os_free(pCon->proto.tcp);
	    os_free(pCon);
	}
	pCon = NULL;
    } 
}

void some_timerfunc(void *arg)
{
    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
    if (pCon == NULL)
    {
	os_printf("pCon NILL\r\n");
	if (wifi_station_get_connect_status () == STATION_CONNECTING)
	{
            pCon_connect();
	    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
	}
	if (pCon == NULL)
	{
	    os_printf("pCon ALLOCATION FAIL\r\n");
	    return;
	}
    }


    pCon->type = ESPCONN_TCP;
    pCon->state = ESPCONN_NONE;
    
    pCon->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
    pCon->proto.tcp->local_port = espconn_port();
    //set up the server remote port
    pCon->proto.tcp->remote_port = 80;

    //set up the remote IP
    uint32_t ip = ipaddr_addr("66.7.217.39"); //IP address for WireGEO.com
    os_memcpy(pCon->proto.tcp->remote_ip, &ip, 4);

    //set up the local IP
    struct ip_info ipconfig;
    wifi_get_ip_info(STATION_IF, &ipconfig);
    os_memcpy(pCon->proto.tcp->local_ip, &ipconfig.ip, 4);

    //register publish_wiregeo_connect_cb that will be called when the
    //connection with the wiregeo is done. In this call back function
    // will actualy do the data sending
    espconn_regist_connectcb(pCon, publish_wiregeo_connect_cb);
    
    pCon_connect();
}

//Do nothing function
static void ICACHE_FLASH_ATTR  user_procTask(os_event_t *events)
{
    os_printf("*", system_get_sdk_version());
    os_delay_us(1000);
}

//Init function 
void ICACHE_FLASH_ATTR user_init()
{
    // Initialize UART0 to use as debug
    uart_div_modify(0, UART_CLK_FREQ / 115200);
	
    // Initialize the GPIO subsystem.
    gpio_init();
	
    
	
// config GPIO12 13 14 16 as normal GPIO
 	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO12);
 	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO15);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO13);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U,FUNC_GPIO14);
	GPIO_OUTPUT_SET(GPIO_ID_PIN(12), 0);
	GPIO_OUTPUT_SET(GPIO_ID_PIN(13), 0);// default low
	GPIO_OUTPUT_SET(GPIO_ID_PIN(14), 0);
	GPIO_OUTPUT_SET(GPIO_ID_PIN(15), 0);
	
	
    os_printf("WireGEO cloud Wi-Fi connect.....\r\n");
    os_printf("SDK version:%s\n", system_get_sdk_version());
	
    const char ssid[32] = SSID;
    const char password[32] = SSID_PASSWORD;

    struct station_config stationConf;
/*
    wifi_set_opmode( STATION_MODE );
    os_memcpy(&stationConf.ssid, ssid, 32);
    os_memcpy(&stationConf.password, password, 32);
    wifi_station_set_config(&stationConf); 
    wifi_station_connect();
*/
	/*
    channels_init ();
	
    //connect to the previous pCon created structure
    pCon_connect();

    //Disarm timer
    os_timer_disarm(&some_timer);

    //Setup timer
    os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);

    //Arm the timer, &some_timer is the pointer 1000 is the fire time in ms
    //0 for once and 1 for repeating timer
    os_timer_arm(&some_timer, 5000, 1);
*/    
    //Start os task
    system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
    
/*
   ETS_GPIO_INTR_DISABLE(); // Disable gpio interrupts
   ETS_GPIO_INTR_ATTACH(interupt_gpio4, 4); // GPIO4 interrupt handler
   PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO4); // Set GPIO4 function
   gpio_output_set(0, 0, 0, GPIO_ID_PIN(4)); // Set GPIO4 as input
   GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(4)); // Clear GPIO4 status
   gpio_pin_intr_state_set(GPIO_ID_PIN(4), 1); // Interrupt on any GPIO4 edge
   ETS_GPIO_INTR_ENABLE(); // Enable gpio interrupts
*/   
#if PLUG_DEVICE
   user_plug_init();
#endif
}