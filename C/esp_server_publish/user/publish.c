#include "publish.h"



void ICACHE_FLASH_ATTR user_esp_platform_dns_found(const char *name, ip_addr_t *ipaddr, void *arg)
{
  struct espconn *pespconn = (struct espconn *)arg;
  if (ipaddr != NULL)
  {
	os_printf("user_esp_platform_dns_found %d.%d.%d.%d\r\n",
		*((uint8 *)&ipaddr->addr), *((uint8 *)&ipaddr->addr + 1),
		*((uint8 *)&ipaddr->addr + 2), *((uint8 *)&ipaddr->addr + 3));
	esp_server_ip = *ipaddr;
	  
	int n;
	for (n=1; n<=PUBLISH_COUNT;n++){
            if ( strcmp(esp_servers[n-1].serv_name, name) == 0 )
	    {
		os_memcpy((uint8 *)&esp_servers[n-1].ip, (uint8 *)&ipaddr->addr, 4);
		/*os_printf("ipaddr->addr 0x%.8X\r\n", (uint32 )ipaddr->addr);
		os_printf("ipaddr->addr 0x%.8X\r\n", (uint32 )esp_servers[n-1].ip);
		os_printf("Found Server %d init:%s, %d.%d.%d.%d\r\n", n, esp_servers[n-1].serv_name,
		*((uint8 *)&esp_servers[n-1].ip), *((uint8 *)&esp_servers[n-1].ip + 1),
		*((uint8 *)&esp_servers[n-1].ip + 2), *((uint8 *)&esp_servers[n-1].ip + 3));*/
		break;
	    }
	}
  }
}

void ICACHE_FLASH_ATTR pCon_connect()
{
    struct espconn *pCon = NULL;
    os_printf("Try espconn_connect\r\n");
    int ret = 0;	
    ret = espconn_connect(pCon);
	
    if(ret == 0) 
	//INFO("espconn_connect OK!\r\n");
	os_printf("espconn_connect OK!\r\n");
    else
    {
	os_printf("espconn_connect FAILED (%d)!\r\n",ret);
	//clean up allocated memory
	if (pCon != NULL)
	{
	    if(pCon->proto.tcp)
                os_free(pCon->proto.tcp);
	    os_free(pCon);
	}
	pCon = NULL;
    } 
}

void ICACHE_FLASH_ATTR  publish_init_name (int n, struct publish_param * info)
{
    info->ip = 0;
    switch (n)
    {
	case 1:
	    os_strcpy(info->serv_name,"cloud.wiregeo.com");
	    break;
	case 2:
	    os_strcpy(info->serv_name,"api.thingspeak.com");
	    break;
	default :
	    os_strcpy(info->serv_name,"user");
    }
}

void ICACHE_FLASH_ATTR  publish_init (int k)
{
    PUBLISH_COUNT = k;
    int n;
    for (n=1; n<=PUBLISH_COUNT;n++){
        publish_init_name (n, & esp_servers[n-1]);
        os_printf("Server%d init:%s\n", n, esp_servers[n-1].serv_name);
    }
}

void some_timerfunc(void *arg)
{
    struct espconn *pCon = NULL;
    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
    if (pCon == NULL)
    {
	os_printf("pCon NILL\r\n");
	if (wifi_station_get_connect_status () == STATION_CONNECTING)
	{
            pCon_connect();
	    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
	}
	if (pCon == NULL)
	{
	    os_printf("pCon ALLOCATION FAIL\r\n");
	    return;
	}
    }
    else
	os_printf("pCon OK!\r\n");

    os_printf("Server timefunc : %s\n", ((struct publish_param * ) arg)->serv_name);
    pCon->type = ESPCONN_TCP;
    pCon->state = ESPCONN_NONE;
    
    pCon->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
    pCon->proto.tcp->local_port = espconn_port();
    //set up the server remote port
    pCon->proto.tcp->remote_port = 80;

    //set up the remote IP thingspeak.com
//    uint32_t ip = 0;
//    os_printf("SET IP 184.106.153.149\r\n");
//    ip = ipaddr_addr("184.106.153.149"); 

//    ip=esp_server_ip.addr;
    //if (esp_server_ip.addr == 0)
    ip_addr_t esp_server_ip;
    if ( ((struct publish_param * ) arg)->ip == 0)
    {
	pCon_connect();
	err_t err = espconn_gethostbyname(pCon,((struct publish_param * ) arg)->serv_name, &esp_server_ip, user_esp_platform_dns_found);
	switch (err){
	    case ESPCONN_OK :
		    os_printf("ESPCONN_OK\r\n");
		break;
	    case ESPCONN_INPROGRESS :
		    os_printf("ESPCONN_INPROGRESS...\r\n");    
		break;
	    default:
		    os_printf("ERR ESPCONN_ARG!\r\n");    
	}
	os_printf("Wait DNS 0x%.8X\r\n", (uint32 *)esp_server_ip.addr);
	return;
    }
    else
    {
    //IP address for WireGEO.comuint32_t ip = ipaddr_addr("66.7.217.39"); //IP address for WireGEO.com
    //os_memcpy(pCon->proto.tcp->remote_ip, &esp_server_ip.addr, 4);
    os_memcpy(pCon->proto.tcp->remote_ip, &((struct publish_param * ) arg)->ip, 4);

    //set up the local IP
    struct ip_info ipconfig;
    wifi_get_ip_info(STATION_IF, &ipconfig);
    os_memcpy(pCon->proto.tcp->local_ip, &ipconfig.ip, 4);

    //register publish_thingspeak_connect_cb that will be called when the
    //connection with the thingspeak is done. In this call back function
    // will actualy do the data sending
    espconn_regist_connectcb(pCon, publish_thingspeak_connect_cb);
    
    pCon_connect();
    }
}