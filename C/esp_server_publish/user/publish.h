#ifndef _PUBLISH_H_
#define _PUBLISH_H_

#include "user_interface.h"
#include "os_type.h"
#include "osapi.h"
#include "espconn.h"
#include <mem.h>

#define PUB_COUNT 3
#include "publish_thingspeak.h"
#include "publish_wiregeo.h"

static int PUBLISH_COUNT = PUB_COUNT;
ip_addr_t esp_server_ip;

static struct espconn *pCon = NULL;

struct publish_param
{
    char serv_name [128];	// имя сервера
    uint32_t ip;		// IP аврес сервера
    //bool en;			// Разрешение
    enum {GET, POST, PUT, DELETE} request_type ;	//Тип запроса
    enum {USER, JSON, XML, CSV} data_type;		//Тип данных
    char request_template [1024];			//Шаблон запроса
};

struct publish_param esp_servers [PUB_COUNT]; // обявляем массив для хранения параметров синхронизации

void user_esp_platform_dns_found(const char *name, ip_addr_t *ipaddr, void *arg);
void pCon_connect();
void publish_init_name (int n, struct publish_param * info);
void publish_init (int k);
void some_timerfunc(void *arg);

#endif