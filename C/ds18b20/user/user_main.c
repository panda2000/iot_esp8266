#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "mem.h"


#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;

void some_timerfunc(void *arg)
{
    os_printf("Read temperature\r\n");
	
    char *tBuf = (char*)os_zalloc(6);
    int temperature;
ds_reset();
ds_write(0xcc,1);
ds_write(0xbe,1);

temperature=(int)ds_read();
temperature=temperature+(int)ds_read()*256;
temperature/=16;
if (temperature>100) temperature-=4096;
ds_reset();
ds_write(0xcc,1);
ds_write(0x44,1);
os_printf(" temperature : %d\r\n", temperature);
	
}

//Do nothing function
static void ICACHE_FLASH_ATTR
user_procTask(os_event_t *events)
{
    os_delay_us(10);
}

//Init function 
void ICACHE_FLASH_ATTR
user_init()
{
    // Initialize UART0 to use as debug
    uart_div_modify(0, UART_CLK_FREQ / 115200);
	
    // Initialize the GPIO subsystem.
    gpio_init();

    ds_init(2); // one off for the DS18B20

    //Disarm timer
    os_timer_disarm(&some_timer);

    //Setup timer
    os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);

    //Arm the timer
    //&some_timer is the pointer
    //1000 is the fire time in ms
    //0 for once and 1 for repeating
    os_timer_arm(&some_timer, 10000, 1);
    
    //Start os task
    system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
}
