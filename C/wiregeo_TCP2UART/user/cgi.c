/*
Some random cgi routines. Used in the LED example and the page that returns the entire
flash as a binary. Also handles the hit counter on the main page.
*/

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Jeroen Domburg <jeroen@spritesmods.com> wrote this file. As long as you retain 
 * this notice you can do whatever you want with this stuff. If we meet some day, 
 * and you think this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */


#include <esp8266.h>
#include "cgi.h"
#include "io.h"
#include "flash_rw.h"
#include "driver/uart.h"

//cause I can't be bothered to write an ioGetLed()
static char currLedState=0;

//Cgi that turns the LED on or off according to the 'led' param in the POST data
int ICACHE_FLASH_ATTR cgiLed(HttpdConnData *connData) {
	int len;
	char buff[1024];
	
	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	len=httpdFindArg(connData->post->buff, "led", buff, sizeof(buff));
	if (len!=0) {
		//currLedState=atoi(buff);
		//ioLed(currLedState);
		DBG1 ("TEST LED -_-_-_-_-_-_-_-_-_-_-");
	}

	httpdRedirect(connData, "led.tpl");
	return HTTPD_CGI_DONE;
}



//Template code for the led page.
int ICACHE_FLASH_ATTR tplLed(HttpdConnData *connData, char *token, void **arg) {
	char buff[128];
	if (token==NULL) return HTTPD_CGI_DONE;

	os_strcpy(buff, "Unknown");
	if (os_strcmp(token, "ledstate")==0) {
		if (currLedState) {
			os_strcpy(buff, "on");
		} else {
			os_strcpy(buff, "off");
		}
	}
	httpdSend(connData, buff, -1);
	return HTTPD_CGI_DONE;
}

static long hitCounter=0;

//Template code for the counter on the index page.
int ICACHE_FLASH_ATTR tplCounter(HttpdConnData *connData, char *token, void **arg) {
	char buff[128];
	if (token==NULL) return HTTPD_CGI_DONE;

	if (os_strcmp(token, "counter")==0) {
		hitCounter++;
		os_sprintf(buff, "%ld", hitCounter);
	}
	httpdSend(connData, buff, -1);
	return HTTPD_CGI_DONE;
}

//Cgi that get Modbus parameters
int ICACHE_FLASH_ATTR cgiGetModbusParam(HttpdConnData *connData) {
	int len;
	char buff[1024];
	
	struct esp_parameters info;
	user_esp_platform_load_param(&info, sizeof(info));

	httpdStartResponse(connData, 200);
	httpdHeader(connData, "Content-Type", "text/json");
	httpdEndHeaders(connData);

	len=os_sprintf(buff, "{\"port\": \"%d\", \"adr\": \"%d\", \"parity\": \"%d\", \"data\": \"%d\", \"stop\": \"%d\", \"baud\": \"%u\"}\n", 
			info.port, info.adr, info.parity, info.data, info.stop, (size_t)info.Baud );
	
	httpdSend(connData, buff, len);
	return HTTPD_CGI_DONE;
}

//Cgi that set Modbus parameters
int ICACHE_FLASH_ATTR cgiSetModbusParam(HttpdConnData *connData) {
	char buff [8];
	
	struct esp_parameters info;
	user_esp_platform_load_param(&info, sizeof(info));
	
	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}
	
	httpdFindArg(connData->post->buff, "port", buff, sizeof(buff));
	info.port = (uint16_t)atoi (&buff[0]);
	os_memset(buff, 0, 8);
	
	// осталось от старой версии с ручной установкой slave адреса ведомого
	//httpdFindArg(connData->post->buff, "slave", buff, sizeof(buff));
	info.adr = 0; //(uint8_t)atoi (&buff[0]);
	//os_memset(buff, 0, 8);
	
	httpdFindArg(connData->post->buff, "parity", buff, sizeof(buff));
	info.parity = (uint16_t)atoi (&buff[0]);
	os_memset(buff, 0, 8);
	
	httpdFindArg(connData->post->buff, "data", buff, sizeof(buff));
	info.data = (uint8_t)atoi (&buff[0]);
	os_memset(buff, 0, 8);
	
	httpdFindArg(connData->post->buff, "stop", buff, sizeof(buff));
	info.stop = (uint16_t)atoi (&buff[0]);
	os_memset(buff, 0, 8);
	
	httpdFindArg(connData->post->buff, "baud", buff, sizeof(buff));
	info.Baud = (uint32_t)atoi (&buff[0]);
	os_memset(buff, 0, 8);
	
	user_esp_platform_save_param(&info, sizeof(info));
	//----------------------------------------------------------------------------

	// Set Modbus RTU parameters
	UART_SetBaudrate	(UART0, info.Baud);  
	UART_SetParity		(UART0, info.parity);
	UART_SetWordLength	(UART0, info.data);
	UART_SetStopBits	(UART0, info.stop);
	
	Slave_adr = info.adr;
	//----------------------------------------------------------------------------
	
	print_ModbusRTU_param(&info);
	print_ModbusTCP_param(&info, 0);
	
	httpdRedirect(connData, "index.tpl");
	return HTTPD_CGI_DONE;

}