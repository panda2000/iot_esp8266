#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
#include <mem.h>

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1

#include "espconn.h" 

#include "publish.h"	// заголовочный файл для синхронизации

//LOCAL struct espconn *pCon = NULL;
LOCAL my_count = 0;
LOCAL your_value = 33;

 os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;



//Do nothing function
static void ICACHE_FLASH_ATTR  user_procTask(os_event_t *events)
{
    os_printf("*", system_get_sdk_version());
    os_delay_us(1000);
}

//Init function 
void ICACHE_FLASH_ATTR user_init()
{
    // Initialize UART0 to use as debug
    uart_div_modify(0, UART_CLK_FREQ / 115200);
	
    os_printf("SDK version:%s\n", system_get_sdk_version());
	
    const char ssid[32] = SSID;
    const char password[32] = SSID_PASSWORD;

    struct station_config stationConf;

    wifi_set_opmode( STATION_MODE );
    os_memcpy(&stationConf.ssid, ssid, 32);
    os_memcpy(&stationConf.password, password, 32);
    wifi_station_set_config(&stationConf); 
    wifi_station_connect();
    //connect to the previous pCon created structure
    pCon_connect();
	
    esp_server_ip.addr = 0;
    // Initialize pablish parameters
    publish_init (3);

    //Disarm timer
    os_timer_disarm(&some_timer);

    //Setup timer
    os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, (void*)& esp_servers[1]);

    //Arm the timer, &some_timer is the pointer 1000 is the fire time in ms
    //0 for once and 1 for repeating timer
    os_timer_arm(&some_timer, 5000, 1);
    
    //Start os task
    system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
}