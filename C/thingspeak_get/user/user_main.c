#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
#include <mem.h>

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1

#include "espconn.h" 
LOCAL struct espconn *pCon = NULL;
LOCAL my_count = 0;
LOCAL your_value = 33;

 os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;

ip_addr_t esp_server_ip;

LOCAL void ICACHE_FLASH_ATTR user_esp_platform_dns_found(const char *name, ip_addr_t *ipaddr, void *arg)
{
  struct espconn *pespconn = (struct espconn *)arg;
  if (ipaddr != NULL)
  {
	os_printf("user_esp_platform_dns_found %d.%d.%d.%d\r\n",
		*((uint8 *)&ipaddr->addr), *((uint8 *)&ipaddr->addr + 1),
		*((uint8 *)&ipaddr->addr + 2), *((uint8 *)&ipaddr->addr + 3));
	esp_server_ip = *ipaddr;
  }
}


static void ICACHE_FLASH_ATTR publish_thingspeak_connect_cb(void *arg)
{
  //INFO("========>publish_thingspeak_connect_cb\r\n");
  my_count++;
  your_value-=5;

  struct espconn *pespconn = (struct espconn *)arg;

  char payload[128];

  char field1[10];
  char field2[10];

  os_sprintf(field1, "%d", your_value);
  os_sprintf(field2, "%d", my_count);

  os_sprintf(payload, "GET /update?api_key=9RPEAG5SJMP3VQBX&field1=%s&field2=%s\r\n", field1, field2);
  os_printf("GET /update?api_key=9RPEAG5SJMP3VQBX&field1=%s&field2=%s\r\n", field1, field2);

  os_printf("ERR = %d", espconn_sent(pespconn, payload, strlen(payload)));
  
}

void pCon_connect()
{
    os_printf("Try espconn_connect\r\n");
    int ret = 0;	
    ret = espconn_connect(pCon);
	
    if(ret == 0) 
	//INFO("espconn_connect OK!\r\n");
	os_printf("espconn_connect OK!\r\n");
    else
    {
	os_printf("espconn_connect FAILED (%d)!\r\n",ret);
	//clean up allocated memory
	if (pCon != NULL)
	{
	    if(pCon->proto.tcp)
                os_free(pCon->proto.tcp);
	    os_free(pCon);
	}
	pCon = NULL;
    } 
}

void some_timerfunc(void *arg)
{
    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
    if (pCon == NULL)
    {
	os_printf("pCon NILL\r\n");
	if (wifi_station_get_connect_status () == STATION_CONNECTING)
	{
            pCon_connect();
	    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
	}
	if (pCon == NULL)
	{
	    os_printf("pCon ALLOCATION FAIL\r\n");
	    return;
	}
    }
    else
	os_printf("pCon OK!\r\n");

    pCon->type = ESPCONN_TCP;
    pCon->state = ESPCONN_NONE;
    
    pCon->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
    pCon->proto.tcp->local_port = espconn_port();
    //set up the server remote port
    pCon->proto.tcp->remote_port = 80;

    //set up the remote IP thingspeak.com
//    uint32_t ip = 0;
//    os_printf("SET IP 184.106.153.149\r\n");
//    ip = ipaddr_addr("184.106.153.149"); 

//    ip=esp_server_ip.addr;
    if (esp_server_ip.addr == 0)
    {
	pCon_connect();
	espconn_gethostbyname(pCon,"api.thingspeak.com", &esp_server_ip, user_esp_platform_dns_found);
	os_printf("Wait DNS 0x%.8X\r\n", (uint32 *)esp_server_ip.addr);
	return;
    }
    else
    {
    //IP address for WireGEO.comuint32_t ip = ipaddr_addr("66.7.217.39"); //IP address for WireGEO.com
    os_memcpy(pCon->proto.tcp->remote_ip, &esp_server_ip.addr, 4);

    //set up the local IP
    struct ip_info ipconfig;
    wifi_get_ip_info(STATION_IF, &ipconfig);
    os_memcpy(pCon->proto.tcp->local_ip, &ipconfig.ip, 4);

    //register publish_thingspeak_connect_cb that will be called when the
    //connection with the thingspeak is done. In this call back function
    // will actualy do the data sending
    espconn_regist_connectcb(pCon, publish_thingspeak_connect_cb);
    
    pCon_connect();
    }
}

//Do nothing function
static void ICACHE_FLASH_ATTR  user_procTask(os_event_t *events)
{
    os_printf("*", system_get_sdk_version());
    os_delay_us(1000);
}

void wifi_config()
{
	wifi_softap_dhcps_start();
	wifi_station_dhcpc_start();
	
// Wifi configuration
    const char ssid[32] = SSID;
    const char password[32] = SSID_PASSWORD;

    struct station_config stationConf;

//Set station mode
wifi_set_opmode(STATIONAP_MODE); //Set softAP + station mode

stationConf.bssid_set = 0;

//Set ap settings
os_memcpy(&stationConf.ssid, ssid, 32);
os_memcpy(&stationConf.password, password, 64);
wifi_station_set_config(&stationConf);
	
	struct ip_info info;
	wifi_get_ip_info(SOFTAP_IF, &info);
	os_printf ("\nIP SOFTAP : %d.%d.%d.%d\n",(info.ip.addr>>0)&0xff, (info.ip.addr>>8)&0xff, (info.ip.addr>>16)&0xff, (info.ip.addr>>24)&0xff );
	wifi_get_ip_info(STATION_IF, &info);
	os_printf ("\nIP STATION : %d.%d.%d.%d\n",(info.ip.addr>>0)&0xff, (info.ip.addr>>8)&0xff, (info.ip.addr>>16)&0xff, (info.ip.addr>>24)&0xff );
	
	os_printf ("\nStatus : %d \n",wifi_station_get_connect_status ());
} 

//Init function 
void ICACHE_FLASH_ATTR user_init()
{
    // Initialize UART0 to use as debug
    uart_div_modify(0, UART_CLK_FREQ / 115200);
	
    os_printf("WireGEO cloud Wi-Fi connect.....\r\n");
    os_printf("SDK version:%s\n", system_get_sdk_version());
/*	
    const char ssid[32] = SSID;
    const char password[32] = SSID_PASSWORD;

    struct station_config stationConf;

    wifi_station_dhcpc_start();

    wifi_set_opmode( STATION_MODE );
    os_memcpy(&stationConf.ssid, ssid, 32);
    os_memcpy(&stationConf.password, password, 32);
    wifi_station_set_config(&stationConf); 
    wifi_station_connect();
	
*/	
    wifi_config();
	
    //connect to the previous pCon created structure
    pCon_connect();
	
    esp_server_ip.addr = 0;

    //Disarm timer
    os_timer_disarm(&some_timer);

    //Setup timer
    os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);

    //Arm the timer, &some_timer is the pointer 1000 is the fire time in ms
    //0 for once and 1 for repeating timer
    os_timer_arm(&some_timer, 5000, 1);
    
    //Start os task
    system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
}