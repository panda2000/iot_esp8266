/*
* Adaptation of Paul Stoffregen’s One wire library to the ESP8266 and
* Necromant’s Frankenstein firmware by Erland Lewin <erland@lewin.nu>
*
* Paul’s original library site:
*   http://www.pjrc.com/teensy/td_libs_OneWire.html
*
* See also http://playground.arduino.cc/Learning/OneWire
*
* Stripped down to bare minimum by Peter Scargill for single DS18B20 or DS18B20P integer read
*/

static int gpioPin;

void ICACHE_FLASH_ATTR ds_init( int gpio );
void ICACHE_FLASH_ATTR ds_reset(void);
static inline void write_bit( int v );
static inline int read_bit(void);
void ICACHE_FLASH_ATTR  ds_write( uint8_t v, int power );
uint8_t ICACHE_FLASH_ATTR ds_read();