local modname = post
local M = {}
_G[modname] = M

function build_post_request(host, uri, param)
 
     request = "POST "..uri.." HTTP/1.1\r\n"..
     "Host: "..host.."\r\n"..
     "Connection: close\r\n"..
     "Content-Type: application/x-www-form-urlencoded\r\n"..
     "Content-Length: "..string.len(param).."\r\n"..
     "\r\n"..
     param
 
     print(request)
     
     return request
end