dofile("wifi_ap.lc")
collectgarbage(); 
ssid=""
password=""
dhcp=0
ip_st="" 
mask_st="" 
gateway_st=""
sv=net.createServer(net.TCP,30) 
sv:listen(80,function(c) 

c:on("receive", function(c, pl)
 answer=pl
 dofile("wifi_parse.lua")
 pl=nil
 answer=""

  if ssid and ssid ~= "" and password and password ~= "" then
   print("ESP8266 connecting to SSID: " .. ssid .. " with PASSWORD: " .. password)
   sv:close()
   dofile ("web_ui_station.lua")
   collectgarbage();
   new_ip = wifi.sta.getip()
   if (new_ip == nil) then new_ip = "0.0.0.0" end
   print("ESP8266 STATION IP now is: " .. new_ip) 
   dofile ("esp_config.lua")
   collectgarbage();
  end      
 buf = ""
 dofile ("user_pass.lua")
 c:send(buf);
 buf = nil;
 c:close();
 collectgarbage();
 end) 
end) 
