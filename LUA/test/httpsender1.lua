local HOST = "cloud.wiregeo.com"
local IP = "66.7.217.39"
local URI = "/sync_p.php"

function build_post_request(host, uri, param)
 
     request = "POST "..uri.." HTTP/1.1\r\n"..
     "Host: "..host.."\r\n"..
     "Connection: close\r\n"..
     "Content-Type: application/x-www-form-urlencoded\r\n"..
     "Content-Length: "..string.len(param).."\r\n"..
     "\r\n"..
     param
 
     print(request)
     
     return request
end
 
local function display(sck,response)
    print ("RECEIVE:")
     print(response)
end
 
local function make_call()
     
     socket = net.createConnection(net.TCP,0)
     socket:on("receive",display)
     socket:connect(80,IP)
 
     socket:on("connection",function(sck) 
       
          local post_request = build_post_request(HOST,URI,"id=ESP-01&data=0001"..t)
          sck:send(post_request)
     end)     
end

print ("POST")
make_call()
--tmr.alarm(0, 60000, 1, function() make_call() end )