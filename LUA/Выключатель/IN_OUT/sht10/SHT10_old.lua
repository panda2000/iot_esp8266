local moduleName = ...
local M = {}
_G[moduleName] = M 
M.sda=4
M.scl=3
 
local function dl()
gpio.write(M.sda, gpio.LOW)
gpio.mode(M.sda, gpio.OUTPUT)
end
 
local function dh()
gpio.mode(M.sda, gpio.INPUT)
gpio.write(M.sda, gpio.HIGH)
end
 
local function cl()
gpio.write(M.scl, gpio.LOW)
end
 
local function ch()
   gpio.write(M.scl, gpio.HIGH)
end
 
local function dr()
gpio.mode(M.sda, gpio.INPUT)
return gpio.read(M.sda)
end
 
local function wait()
for i = 1, 100 do
 tmr.delay(10000)
 if dr() == gpio.LOW then
  return true
 end
end
return false
end
 
local function read(bits)
local val = 0
 for i = 0, bits-1 do
  ch()
  val = val * 2 + dr()
  cl()
 end
return val
end
 
local function write(val,bits)
 bits = bits -1
 for i = 0, bits do
 if bit.band(val, 2 ^ (bits-i)) == 0 then
  dl()
 else
  dh()
 end
  ch(); cl()
 end
end
 
local function read_cmd(cmd)
 dh() ch() dl() cl() ch() dh() cl()
 write(cmd,8)
 ch(); cl()
 if not wait() then
  return nil
 end
 dh()
 local val = read(8)
 dh() dl() ch() cl() dh()
 val = val * 256 + read(8) 
 dh() ch() cl()
 return val
end
 
function M.init(d, l)
 if d ~= nil then
  sda = d
 end
 if l ~= nil then
  scl = l
 end
 gpio.mode(sda, gpio.INPUT)
 gpio.mode(scl, gpio.OUTPUT)
 print("SHT1x init done SDA",sda,"SCL",scl)
end
 
function M.get_raw_temperature()
 return read_cmd(3)
end
 
function M.get_raw_humidity()
 return read_cmd(5)
end
 
return M
