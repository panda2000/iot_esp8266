#ifndef FLASH_RW_H
#define FLASH_RW_H

#define ESP_PARAM_START_SEC   0x11
// SPI_FLASH_SEC_SIZE 4096 =  0x1000 = 4 Кбайта


#define ESP_PARAM_SAVE_0    0
#define ESP_PARAM_SAVE_1    1
#define ESP_PARAM_FLAG      2


uint8 Slave_adr;

struct esp_platform_sec_flag_param {
    uint8 flag;
    uint8 pad[3];
} temp;	// защищённая область памяти

struct esp_parameters{
  uint8_t flag; 	// флаг для защиты параметров
  // параметры Modbus RTU
  uint8_t adr;		// Modbus slave адрес 1-247
  uint8_t parity;	// 0х0/0х1/0х2
  uint8_t data; 	// data_bits 0х0/0х1/0х2/0х3
  uint8_t stop;		// stop_bits 0х1/0х2/0х3
  uint8_t CRLF;		// заглушка для выравнивания
  // параметры Modbus TCP
  uint16_t port; 	// сетевой порт для подключения по умолчанию 502
  uint32_t Baud;	// Скорость 300 - 3686400
  // статические сетевые параметры устройства
  uint32_t ip;		// IP адрес
  uint32_t mask;	// Маска
  uint32_t gw;		// Шлюз
};	 // Сохраняемые параметры 32*6 = 192 бита = 24 байта

void ICACHE_FLASH_ATTR user_esp_platform_load_param(void *param, uint16 len);
void ICACHE_FLASH_ATTR user_esp_platform_save_param(void *param, uint16 len);

void ICACHE_FLASH_ATTR print_ModbusRTU_param(struct esp_parameters *info);
void ICACHE_FLASH_ATTR print_ModbusTCP_param(struct esp_parameters *info, uint16 def_port);
void ICACHE_FLASH_ATTR print_TCP_param(struct esp_parameters *info);

#endif