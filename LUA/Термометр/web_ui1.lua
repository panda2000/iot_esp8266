dofile("wifi_ap.lc")
collectgarbage(); 
ssid = ""
password = ""
sv=net.createServer(net.TCP,30) 
sv:listen(80,function(c) 

    c:on("receive", function(c, pl)  
    print("|GET|")
    print(pl)
    print("--------------------------")
    local ssid_start,ssid_end=string.find(pl,"SSID=") 
    if ssid_start and ssid_end then 
        pl = string.sub(pl,ssid_start)
        amper1_start, amper1_end =string.find(pl,"&", ssid_end+1) 
        if amper1_start and amper1_end then 
            http_start, http_end =string.find(pl,"HTTP/1.1", ssid_end+1) 
            if http_start and http_end then 
                ssid=string.sub(pl,ssid_end+1, amper1_start-1) 
                password=string.sub(pl,amper1_end+10, http_start-2) 
                print("ESP8266 connecting to SSID: " .. ssid .. " with PASSWORD: " .. password) 
                if ssid and ssid ~= "" and password and password ~= "" then 
                    sv:close()
                    dofile ("web_ui_station.lua")
                    collectgarbage();
                    new_ip = wifi.sta.getip()
                    if (new_ip == nil) then new_ip = "0.0.0.0" end
                    print("ESP8266 STATION IP now is: " .. new_ip) 
                    dofile ("esp_config.lua")
                    collectgarbage();
                end 
            end 
       end 
    end     
    -- this is the web page that requests the SSID and password from the user
    buf = ""
    dofile ("user_pass.lua")
    c:send(buf);
    buf = nil;
    c:close();
    collectgarbage();
    
    end) 
 end) 
--sv:close()
