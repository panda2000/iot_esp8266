#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
//#include "lwip/stats.h"

#include "espconn.h"

#include "TCPport.h"

#define TIMEOUT 60*60*12 // yes. 12h timeout. 

static struct espconn serverConn;
static esp_tcp serverTcp;



LOCAL void ICACHE_FLASH_ATTR
tcp_disconcb(void *arg) {
    struct espconn *pespconn = (struct espconn *) arg;

    //os_printf("tcp connection disconnected\n");
}

LOCAL void ICACHE_FLASH_ATTR
tcpclient_sent_cb(void *arg) {
    struct espconn *pespconn = (struct espconn *) arg;

    //os_printf("SentCb on conn %p\n", pespconn);
    espconn_disconnect(pespconn);
}

LOCAL void ICACHE_FLASH_ATTR
tcp_recvcb(void *arg, char *pusrdata, unsigned short length)
{
    struct espconn *pespconn = (struct espconn *) arg;
	/*
	os_printf(">'%s' ", pusrdata);

        int i;
        for(i = 0; i < length; i++) {
            os_printf("0x%02X ", pusrdata[i]);
        }
        os_printf("\n");
	*/
	Poll(pusrdata);
	TCP_send(pusrdata, strlen(pusrdata)); 
    
}

void ICACHE_FLASH_ATTR
TCP_send(char *data, unsigned short length)
{
	/*
	os_printf(">'%s' ", data);

        int i;
        for(i = 0; i < length; i++) {
            os_printf("0x%02X ", data[i]);
        }
        os_printf("\n");
	*/
	espconn_send(&serverConn, data, length); 
}


LOCAL void ICACHE_FLASH_ATTR
tcpserver_connectcb(void *arg)
{
    struct espconn *pespconn = (struct espconn *)arg;

   // os_printf("tcp connection established\n");

    espconn_regist_recvcb(pespconn, tcp_recvcb);
    // espconn_regist_reconcb(pespconn, tcpserver_recon_cb);
    espconn_regist_disconcb(pespconn, tcp_disconcb);
    espconn_regist_sentcb(pespconn, tcpclient_sent_cb);
}

bool ICACHE_FLASH_ATTR
TCPPortInit(int port)
{
	
	serverConn.type=ESPCONN_TCP;
	serverConn.state=ESPCONN_NONE;
	serverTcp.local_port=port;
	serverConn.proto.tcp=&serverTcp;

	espconn_regist_connectcb(&serverConn, tcpserver_connectcb);
	espconn_accept(&serverConn);
	espconn_regist_time(&serverConn, TIMEOUT, 0);	
	//os_printf("[%s] initializing shell!\n", __func__);
        return TRUE;   
    } 