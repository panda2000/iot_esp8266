<?php
//$time_start = microtime(true);
error_reporting(0);
ignore_user_abort(true);
//header('Content-Type: text/plain; charset=utf-8');
header_remove("X-Powered-By");

require('db.php');
require('func.php');

mysql_query('SET time_zone="'.date('P').'"',$db);

if (getenv('REMOTE_ADDR') == '127.0.0.1')
{
	$_POST['id'] = 'STAND';
	$_POST['time'] = '2014-05-19 00:03:13';
	$_POST['action'] = 'set';
	//$_POST['data'] = 'C8000442840000'; //'C80A04C0133333';
	$_POST['data'] = '0D0C01';
	//'000078000100000200020B00020C00020D00030E01030F010310C802110002120065131C7F651411C2661720636718063A6800009668000A9B68000A9B'; //'00001A0001000002006403014664040151640501586406015A6407013564080013640900E1640A0000020B00020C00020D00030E01030F010310C802110002120065131C7F651411C2011501011601661720636718063A';
	/*$_POST['data'] = 'C8000442840000
C8010400000000
C8020400000000
C8030400000000
C8040442840000
C8050400000000
C8060400000000
C8070400000000
C8080400000000
C8090400000000
C80A0400000000
C80B0400000000
C80C0400000000
C80D0400000000
C80E0400000000
C80F0400000000
C8100400000000
C8110400000000
C8120400000000
C8130400000000
C8140400000000
C8150400000000
C8160400000000
C8170400000000
C8180400000000
C8190400000000
C81A0400000000
C81B0400000000
C81C040000000E';*/
}

$DEVICE_CODE = $_POST['id'];
if (empty($DEVICE_CODE)) die('error=101');

//Counter total
$c = file_get_contents('sync/counter_total_'.md5($DEVICE_CODE).'.txt');
if (empty($c)) $c = 0;
$c++;
file_put_contents('sync/counter_total_'.md5($DEVICE_CODE).'.txt',$c);

$devres = mysql_query('SELECT `object_id`,`device_flags`,`device_dt` FROM `sh_devices` WHERE (LCASE(`system_name`) = "'.mysql_real_escape_string(strtolower($DEVICE_CODE),$db).'") AND (`device_type` = 1)',$db);
$DEVICE_ID = mysql_result($devres,0,'object_id');
$DEVICE_DT = mysql_result($devres,0,'device_dt');
if (empty($DEVICE_ID)) die('error=102');

//Counter sync
$c = file_get_contents('sync/counter_sync_'.md5($DEVICE_CODE).'.txt');
if (empty($c)) $c = 0;
$c++;
file_put_contents('sync/counter_sync_'.md5($DEVICE_CODE).'.txt',$c);

/*if ($DEVICE_CODE = 'demonstration')
{
	$_POST['data'] = str_replace('0D0C00','0D0C01',$_POST['data']);
	$_POST['data'] = str_replace('360800','360801',$_POST['data']);
	$_POST['data'] = str_replace('0E0E00','0E0E01',$_POST['data']);
}*/

//+Логирование входного потока
ob_start();
foreach ($_POST as $key => $val)
	$_POST[$key] = stripslashes($val);
print_r($_POST);
$buf = ob_get_contents();
ob_end_clean();
file_put_contents('sync/in_'.md5($DEVICE_CODE).'.txt',$buf);
//-Логирование входного потока

$dev_flags = mysql_result($devres,0,'device_flags');
$res = mysql_query('SELECT `account_id`,`object_full_name` FROM `sh_objects` WHERE (`object_id` = '.$DEVICE_ID.') AND (`object_type` = 3)',$db);
$ACCOUNT_ID = mysql_result($res,0,'account_id');
$DEVICE_FULL_NAME = mysql_result($res,0,'object_full_name');
if (empty($ACCOUNT_ID)) die('error=103');

$devices = array(
	'TERMO8' => 0,
	'D_INPUT' => 1,
	'D_OUT' => 2,	
	'DIMMER' => 3,
	'HUMID' => 4,
	'AIR' => 6,
	'SMOKE' => 7,
	'DOOR' => 8,
	'POWER' => 9,
	'SIREN' => 10,
	'AIRGAS' => 12,
	'D_MOTION' => 13,
	'D_LEAK' => 14,
	'MOTION' => 50,
	'LEAK' => 51,
	'P_RESET' => 52,
	'SIGNALING' => 53,
	'FIRE_LINE' => 54,
	'INPUT_ADC' => 100,
	'COUNTER' => 101,
	'IR_IN' => 102,
	'IR_OUT' => 103,
	'TERMO16' => 104,
	'N2UNSGN0' => 110,
	'N2SGN0' => 111,
	'N2UNSHN1' => 112,
	'N2SGN1' => 113,
	'N2UNSGN2' => 114,
	'N2SGN2' => 115,
	'IEEE754FLT' => 200
);
$devices_name = array(
	'TERMO8' => 'Термометр',
	'D_INPUT' => 'ЦифровойВход',
	'D_OUT' => 'ЦифровойВыход',
	'DIMMER' => 'ОсветительныйДиммер',
	'HUMID' => 'ДатчикВлажности',
	'AIR' => 'КачествоВоздуха',
	'SMOKE' => 'Задымлённость',
	'DOOR' => 'ДатчикДвери',
	'POWER' => 'ДатчикНапряжения',
	'SIREN' => 'Сирена',
	'AIRGAS' => 'ГазВВоздухе',
	'D_MOTION' => 'ДатчикДвиженияДискр',
	'D_LEAK' => 'ДатчикПротечкиДискр',
	'MOTION' => 'ДатчикДвижения',
	'LEAK' => 'ДатчикПротечки',
	'P_RESET' => 'АппаратныйСброс',
	'SIGNALING' => 'Сигнализация',
	'FIRE_LINE' => 'ПожарнаяЛиния',
	'INPUT_ADC' => 'АналоговыйВход',	
	'COUNTER' => 'СчётчикИмпульсов',
	'IR_IN' => 'ПриёмникИК',
	'IR_OUT' => 'ИзлучательИК',
	'TERMO16' => 'Термометр',
	'N2UNSGN0' => 'ЧислоБеззнак2БТ0',
	'N2SGN0' => 'ЧислоЗнак2БТ0',
	'N2UNSHN1' => 'ЧислоБеззнак2БТ1',
	'N2SGN1' => 'ЧислоЗнак2БТ1',
	'N2UNSGN2' => 'ЧислоБеззнак2БТ2',
	'N2SGN2' => 'ЧислоЗнак2БТ2',
	'IEEE754FLT' => 'ЗначениеIEEE754'
);
$send_back = array(
	0 => false,
	1 => false,
	2 => true,
	3 => false,
	4 => false,
	6 => false,
	7 => false,
	8 => false,
	9 => false,
	10 => false,
	12 => false,
	13 => true,
	14 => true,
	50 => true,
	51 => true,
	52 => true,
	53 => true,
	54 => true,
	100 => false,
	101 => false,
	102 => false,
	103 => false,
	104 => false,
	110 => true,
	111 => true,
	112 => true,
	113 => true,
	114 => true,
	115 => true,
	200 => true
);

$data = $_POST['data'];
$data = str_replace(array(' ',chr(13),chr(10)),'',$data);
$data_raw = array();
$data_real = array();
for ($i = 0; $i < ceil(strlen($data)/2); $i++)
{
	$data_raw[] = substr($data,$i*2,2);
	$data_real[] = hexdec(substr($data,$i*2,2));
}

$dev_data = array();
$i = 0;
while ($i < count($data_real)):
	$dev_name = $data_real[$i];
	$dev_id = $data_real[$i+1];
	if ($dev_name <= 99)
		$len = 1;
	else if ($dev_name <= 199)
		$len = 2;
	else if ($dev_name <= 255)
		$len = $data_real[$i+2]*2;
	$dev_value = '';
	for ($j = 0; $j < $len; $j++)
		$dev_value .= $data_raw[$i+2+$j];
	if ($dev_name >= 200)
		$dev_value = substr($dev_value,2); //Т.к. первые 2 символа — длина
	$dev_code = array_search($dev_name,$devices);
	$dev_value_orig = $dev_value;
	$dev_value = hexdec($dev_value);
	$dev_data[$dev_id] = array($dev_code,$dev_id,$dev_value,$dev_value_orig);
	$i += 2+$len;
endwhile;

//print_r($dev_data);

//Пост-обработка значений (отрицательные и т.п.)
foreach ($dev_data as $key => $data)
{
	if (in_array($data[0],array('TERMO16')))
	{
		$hdb = decbin($data[2]);
		$hdb = str_pad($hdb,16,'0',STR_PAD_LEFT);
		if ($hdb[0] == 1) $dec = bindec($hdb)-pow(2,16); else $dec = bindec($hdb);
		$dev_data[$key][2] = number_format($dec/100,1);
	}
	else if (in_array($data[0],array('N2UNSGN0','N2SGN0','N2UNSGN1','N2SGN1','N2UNSGN2','N2SGN2')))
	{
		$hdb = decbin($data[2]);
		$hdb = str_pad($hdb,16,'0',STR_PAD_LEFT);
		if ((in_array($data[0],array('N2SGN0','N2SGN1','N2SGN2'))) && ($hdb[0] == 1)) $dec = bindec($hdb)-pow(2,16); else $dec = bindec($hdb);
		if (in_array($data[0],array('N2UNSGN1','N2SGN1'))) $dec = number_format($dec/10,1);
		if (in_array($data[0],array('N2UNSGN2','N2SGN2'))) $dec = number_format($dec/100,2);
		$dev_data[$key][2] = $dec;
	}
	else if ($data[0] == 'IEEE754FLT')
		$dev_data[$key][2] = hex2float($data[3]);
}

if (isset($_GET['debug2'])) print_r($dev_data);

$NOTIFIES = array();

//ЛОГИКА

/*
if ($DEVICE_CODE == 'NetAlarm_MOD')
{
	$dev_data[12][2] = ($dev_data[8][2] > 100)?1:0;
}
else if ($DEVICE_CODE == 'realserverroom1')
{
	//Инверсия напряжения
	//if ($dev_data[9][2] == 1) $dev_data[9][2] = 0; else $dev_data[9][2] = 1;
	/*$dev_data[2][2] = hum_conv($dev_data[2][2]);
	$dev_data[3][2] = hum_conv($dev_data[3][2]);*/
	//Уведомления
	/*$temp0 = $dev_data[0][2];
	$temp1 = $dev_data[1][2];
	$voltage = $dev_data[9][2];
	$hum1 = $dev_data[2][2];
	$hum2 = $dev_data[3][2];
	/*if (($temp0 < 18) || ($temp0 > 26)) $NOTIFIES['УведТемп0'] = true;
	if (($temp1 < 18) || ($temp1 > 26)) $NOTIFIES['УведТемп1'] = true;
	if ($voltage == 0) $NOTIFIES['УведНапр'] = true;
	if ($hum1 > 80) $NOTIFIES['УведВлажн0'] = true;
	if ($hum2 > 80) $NOTIFIES['УведВлажн1'] = true;*/
//}
//ЛОГИКА

//Уведомления
if (count($NOTIFIES) > 0)
{
	$nnames = array();
	foreach ($NOTIFIES as $key => $val)
	{
		$NOTIFIES[$key] = date('Y-m-d H:i:00',time());
		$nnames[] = mysql_real_escape_string($key,$db);
	}
	$res = mysql_query('SELECT `object_id`, `notify_name` FROM `sh_notifies` WHERE (`account_id` = '.$ACCOUNT_ID.') AND (`notify_name` IN ("'.implode('","',$nnames).'"))',$db);
	$arr = array();
	while ($row = mysql_fetch_assoc($res))
		$arr[$row['notify_name']] = $row['object_id'];
	$ins = array();
	foreach ($NOTIFIES as $nn => $nt)
		if (isset($arr[$nn]))
			$ins[] = '('.$arr[$nn].',"'.$nt.'")';
	mysql_query('INSERT IGNORE INTO `sh_notifies_log` (`notify_id`,`notify_time`) VALUES '.implode(',',$ins),$db);
}

$ins = array();
foreach ($dev_data as $dev)
{
	$dsname = $dev[0].$dev[1];
	$dn = $devices_name[$dev[0]];
	if (strlen($dn) == 0) $dn = 'Устройство';
	$dname = $dn.$dev[1];
	$dfname = $DEVICE_FULL_NAME.'.'.$dname;
	$dvalue = $dev[2];
	$vname = $dfname.'.Value';
	$ins[] = '("'.mysql_real_escape_string($dname,$db).'","'.mysql_real_escape_string($dfname,$db).'","'.mysql_real_escape_string($dsname,$db).'","'.mysql_real_escape_string($dvalue,$db).'","'.mysql_real_escape_string($vname,$db).'")';
}
$device_count_total = count($ins);

//Отправка всего присланного во временную таблицу
mysql_query("CREATE TEMPORARY TABLE `sh_sync_dev` (
`object_id` int(11) NOT NULL,
`dev_name` varchar(255) collate utf8_bin NOT NULL,
`dev_full_name` varchar(255) collate utf8_bin NOT NULL,
`dev_sys_name` varchar(255) collate utf8_bin NOT NULL,
`dev_value` varchar(255) collate utf8_bin NOT NULL,
`variable_name` varchar(255) collate utf8_bin NOT NULL,
`variable_id` int(11) NOT NULL
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin",$db);
mysql_query('INSERT INTO `sh_sync_dev` (`dev_name`,`dev_full_name`,`dev_sys_name`,`dev_value`,`variable_name`) VALUES '.implode(',',$ins),$db);

//Обновление имеющегося
mysql_query('UPDATE `sh_sync_dev` AS SD INNER JOIN `sh_objects` AS O ON (O.`object_full_name` = SD.`dev_full_name`) SET SD.`object_id` = O.`object_id` WHERE (O.`account_id` = '.$ACCOUNT_ID.') AND (`object_type` = 3)',$db);
$device_count_exist = mysql_affected_rows($db);

//Обновление и логирование значений переменных
mysql_query('UPDATE `sh_sync_dev` AS SD INNER JOIN `sh_objects` AS O ON (O.`object_full_name` = SD.`variable_name`) SET SD.`variable_id` = O.`object_id` WHERE (O.`account_id` = '.$ACCOUNT_ID.') AND (`object_type` = 5)',$db);
$var_count_exist = mysql_affected_rows($db);
if ($var_count_exist > 0)
{
	mysql_query("CREATE TEMPORARY TABLE `sh_sync_vars` (
  `var_id` int(11) NOT NULL,
  `var_name` varchar(255) collate utf8_bin NOT NULL,
  `var_value` varchar(64) collate utf8_bin NOT NULL,
  `var_value_old` varchar(64) collate utf8_bin NOT NULL,
  `var_changed` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `var_changer` tinyint(4) NOT NULL default '0',
  `var_type` enum('A','B','N') collate utf8_bin NOT NULL,
  `var_logging` varchar(5) collate utf8_bin,
  `var_log_now` tinyint(4) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin",$db);
	mysql_query('INSERT INTO `sh_sync_vars` (`var_id`,`var_name`,`var_value`,`var_changed`,`var_changer`) SELECT `variable_id`,`variable_name`,`dev_value`,NOW(),2 FROM `sh_sync_dev` WHERE (`variable_id` > 0)',$db);
	mysql_query('UPDATE	`sh_sync_vars` AS SV
	LEFT JOIN `sh_variables` AS V ON (V.`variable_full_name` = SV.`var_name`)
	SET
	SV.`var_id` = V.`object_id`, SV.`var_logging` = V.`variable_logging`, SV.`var_type` = V.`variable_type`, SV.`var_value_old` = V.`variable_last_log_value`
	WHERE (V.`account_id` = '.$ACCOUNT_ID.')',$db);
	//Если значение поменялось на сервере (changer=1), то игнорируем значение от контроллера и отправляем ему своё, но тут же меняем changer на 2, чтобы в следующий раз уже получить значение с контроллера. Время никак не учитывается.
	mysql_query('UPDATE `sh_variables` AS V
	INNER JOIN `sh_sync_vars` AS SV ON (SV.`var_id` = V.`object_id`)
	SET V.`variable_value` = IF(V.`variable_changer` = 1, V.`variable_value`, SV.`var_value`), V.`variable_changed` = SV.`var_changed`, V.`variable_changer` = 2, SV.`var_log_now` = 2
	WHERE (SV.`var_id` > 0)',$db);
	mysql_query('UPDATE `sh_sync_vars` SET
	`var_log_now` = 1
	WHERE (`var_id` > 0) AND (`var_log_now` = 2) AND (`var_logging` != "") AND (
	((`var_type` IN ("A","B")) AND (`var_value_old` != `var_value`))
	OR
	((`var_type` = "N") AND (ABS(CAST(`var_value` AS DECIMAL(10,3))-CAST(`var_value_old` AS DECIMAL(10,3))) >= CAST(`var_logging` AS DECIMAL(10,3))))
	)',$db);
	if (mysql_affected_rows($db) > 0)
	{
		mysql_query('INSERT INTO `sh_var_changes` (`variable_id`,`variable_value`) SELECT `var_id`,`var_value` FROM `sh_sync_vars` WHERE (`var_log_now` = 1)',$db);
		mysql_query('UPDATE `sh_variables` AS V SET V.`variable_last_log_value` = V.`variable_value` WHERE (V.`object_id` IN (SELECT `var_id` FROM `sh_sync_vars` WHERE (`var_log_now` = 1)))',$db);
	}
}

mysql_query('DELETE FROM `sh_sync_dev` WHERE (`object_id` > 0)',$db);

//Добавление новых устройств
if ($device_count_exist < $device_count_total)
{
	mysql_query('INSERT INTO `sh_objects` (`parent_id`,`account_id`,`object_type`,`object_name`,`object_full_name`) SELECT '.$DEVICE_ID.','.$ACCOUNT_ID.',3,`dev_name`,`dev_full_name` FROM `sh_sync_dev`',$db);
	mysql_query('UPDATE `sh_sync_dev` AS SD INNER JOIN `sh_objects` AS O ON (O.`object_full_name` = SD.`dev_full_name`) SET SD.`object_id` = O.`object_id` WHERE (O.`account_id` = '.$ACCOUNT_ID.')',$db);
	mysql_query('INSERT INTO `sh_devices` (`object_id`,`parent_id`,`device_name`,`system_name`,`device_type`,`device_updated`) SELECT `object_id`,'.$DEVICE_ID.',`dev_name`,`dev_sys_name`,2,NOW() FROM `sh_sync_dev`',$db);
}

//Добавление новых переменных
if ($var_count_exist < $device_count_total)
{
	mysql_query('INSERT INTO `sh_objects` (`parent_id`,`account_id`,`object_type`,`object_name`,`object_full_name`) SELECT `object_id`,'.$ACCOUNT_ID.',5,"Value",`variable_name` FROM `sh_sync_dev`',$db);
	mysql_query('UPDATE `sh_sync_dev` AS SD INNER JOIN `sh_objects` AS O ON (O.`object_full_name` = SD.`variable_name`) SET SD.`variable_id` = O.`object_id` WHERE (O.`account_id` = '.$ACCOUNT_ID.') AND (`object_type` = 5)',$db);
	mysql_query('INSERT INTO `sh_variables` (`object_id`,`account_id`,`variable_full_name`,`variable_type`,`variable_value`,`variable_changed`,`variable_changer`) SELECT `variable_id`,'.$ACCOUNT_ID.',`variable_name`,"N",`dev_value`,NOW(),2 FROM `sh_sync_dev`',$db);
}

mysql_query('UPDATE `sh_devices` SET `device_updated` = NOW() WHERE (`object_id` = '.$DEVICE_ID.')',$db);

//ВНЕШНЯЯ ЛОГИКА
ob_start();
print 'PROCESS BEGIN'."\r\n";
$_GET['debug'] = true;
include('process.php');
print 'PROCESS END'."\r\n";
$pbuf = ob_get_contents();
ob_end_clean();

//Формирование ответа
$res = mysql_query('SELECT
D.`system_name` AS DEV_NAME, V.`variable_value` AS DEV_VALUE
FROM `sh_devices` AS D
INNER JOIN `sh_objects` AS O ON (O.`parent_id` = D.`object_id`)
INNER JOIN `sh_variables` AS V ON (V.`object_id` = O.`object_id`)
WHERE (D.`parent_id` = '.$DEVICE_ID.') AND (D.`device_type` = 2) AND (O.`object_type` = 5)',$db);
$data = array();
while ($row = mysql_fetch_assoc($res))
{
	$dname = $row['DEV_NAME'];
	$dev_name = '';
	$dev_id = '';
	foreach ($devices as $key => $val)
		if ((substr($dname,0,strlen($key)) == $key) && (strlen($key) > strlen($dev_name)))
		{
			$dev_name = $key;
			$dev_id = substr($dname,strlen($key));
		}	
	$t = array('dev_name' => $devices[$dev_name], 'dev_id' => $dev_id, 'dev_value' => $row['DEV_VALUE']);;
	$data[] = $t;
}

if (isset($_GET['debug2'])) print_r($data);

// +Логика при формировании ответа
foreach ($data as $key => $arr)
{
	//Триггеры
	if (in_array($arr['dev_name'],array(13,14,50,51)))
		$data[$key]['dev_value'] = 0;
		
	if ($arr['dev_name'] == 200)
		$data[$key]['dev_value'] = float2hex($data[$key]['dev_value']);
}
// -Логика при формировании ответа

if (isset($_GET['debug2'])) print_r($data);

$data_enc = '';
foreach ($data as $arr)
{
	if ($send_back[$arr['dev_name']] == false) continue;
	$hex1 = str_pad(dechex($arr['dev_name']),2,'0',STR_PAD_LEFT);
	$val = dechex($arr['dev_value']);
	if (strlen($val) % 2 == 1) $val = '0'.$val;
	if ($arr['dev_name'] <= 99)
	{
		if (strlen($val) > 2)
			$val = substr($val,-2);
		else
			$val = str_pad($val,2,'0',STR_PAD_LEFT);
	}
	else if ($arr['dev_name'] <= 199)
	{
		if (strlen($val) > 4)
			$val = substr($val,-4);
		else
			$val = str_pad($val,4,'0',STR_PAD_LEFT);
	}
	else
	{
		if ($arr['dev_name'] == 200) $val = $arr['dev_value']; //Там уже HEX
		if ($arr['dev_name'] == 200) $len = 4;
		else $len = ceil(strlen($val)/2);
		if (strlen($val) < $len*2) $val = str_pad($val,$len*2,'0',STR_PAD_LEFT);
		$val = str_pad(dechex($len),2,'0',STR_PAD_LEFT).$val;
	}
	$hex2 = str_pad(dechex($arr['dev_id']),2,'0',STR_PAD_LEFT);
	$data_enc .= $hex1.$hex2.$val;
}

/*$time_end = microtime(true);
$execution_time = number_format($time_end - $time_start,3);
file_put_contents('sync/sync_p_time.txt',$DEVICE_ID.';'.$execution_time."\r\n",FILE_APPEND);*/

ob_start();

print 'id='.$DEVICE_CODE.'&data='.strtoupper($data_enc).'&dt='.$DEVICE_DT; //&time='.date('Y-m-d H:i:s').'&action=set
//print "\r\n\r\n";
//print_r($data);

//+Логирование выходного потока
$buf = ob_get_contents();
header('Content-Length: '.strlen($buf));
ob_end_flush();
file_put_contents('sync/out_'.md5($DEVICE_CODE).'.txt',$buf."\r\n".$pbuf);
//-Логирование выходного потока
?>