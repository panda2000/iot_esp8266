print("START",tmr.now())
id="ESP-01"
dt=0.5*20
w1_pin=12
sda_pin=13
scl_pin=14
esp_pin={["DS"]={},["GPIN"]={0,2,5,4},["GPOUT"]={12,13,14,16},["LEAK"]={},["SHT"]={},["ADC"]={17}}
-- ESP-01 GPIO Mapping
esp_gpio={3,10,4,9,2,1,"","","",11,12,"", 6, 7, 5, 8, 0,17}
esp_data={"","","","","","","","","","","","","","","","",""}
request=""
answer=""
send_back="|2|13|14|50|51|52|53|54|110|111|112|113|114|115|200|"
--dofile("uart.lua")    -- test config mode
collectgarbage()
dofile ("config.lua")   --read config file
collectgarbage()
if request~="CONFIG MODE" then
print("MAIN",tmr.now())
print("HEAP0=",node.heap())
dofile("initio.lua")
collectgarbage()
dofile("script.lua")
collectgarbage()
dofile("httpsender.lc")
collectgarbage()
print("HEAP3=",node.heap())
end
