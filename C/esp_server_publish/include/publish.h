#ifndef _PUBLISH_H_
#define _PUBLISH_H_
#include "os_type.h"

struct publish_param
{
    char serv_name [128];	// имя сервера
    uint32_t ip;		// IP аврес сервера
    //bool en;			// Разрешение
    enum {GET, POST, PUT, DELETE} request_type ;	//Тип запроса
    enum {USER, JSON, XML, CSV} data_type;		//Тип данных
    char request_template [1024];			//Шаблон запроса
};

void publish_init (int n, struct publish_param * info);


#endif