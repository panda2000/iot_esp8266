local moduleName = ...
local M = {}
_G[moduleName] = M
 
local sda = 4
local scl = 3
 
local function sd(val)
print("sd()")
if (val==0 or val==nil) then
gpio.write(M.sda, gpio.LOW)
gpio.mode(M.sda, gpio.OUTPUT)
else
gpio.mode(M.sda, gpio.INPUT)
gpio.write(M.sda, gpio.HIGH)
end end
 
local function sc(val)
print("sc()")
if (val==0 or val==nil) then
gpio.write(M.scl, gpio.LOW)
else
gpio.write(M.scl, gpio.HIGH)
end end
 
local function dr()
print("dr()")
 gpio.mode(sda, gpio.INPUT)
 return gpio.read(sda)
end
 
local function wait()
 for i = 1, 100 do
  tmr.delay(10000)
  if dr() == gpio.LOW then
   return true
  end
 end
 return false
end
 
local function read_byte()
 local val = 0
 for i = 0, 7 do
  sc(1)
  val = val * 2 + dr()
  sc(0)
 end
 return val
end
 
local function write_byte(val)
 for i = 0, 7 do
  if bit.band(val, 2 ^ (7-i)) == 0 then
   sd(0)
  else
   sd(1)
  end
  sc(1); sc(0)
 end
end
 
local function read_cmd(cmd)
-- print("read_cmd(",cmd,")")
 sd(1) sc(1) sd(0) sc(0) sc(1) sd(1) sc(0)
-- print("write_byte(",cmd,")")
 write_byte(cmd)
 sc(1); sc(0)
 if not wait() then
  return nil
 end
 sd(1)
 local val = read_byte() 
 sd(1) sd(0) sc(1) sc(0) sd(1)
 val = val * 256 + read_byte()
 sd(1) sc(1) sc(0)
 return val
end
 
function M.init(d, l)
 print("M.init(d, l)")
 if d ~= nil then
  sda = d
 end
 if l ~= nil then
  scl = l
 end
 gpio.mode(sda, gpio.INPUT)
 gpio.mode(scl, gpio.OUTPUT)
 print("SHT1x init done")
end
 
function M.get_raw_temperature()
 print("get_raw_temperature()")
 return read_cmd(3)
end
 
function M.get_raw_humidity()
 print("get_raw_humidity()")
 return read_cmd(5)
end
 
return M
