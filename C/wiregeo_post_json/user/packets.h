#ifndef _PACKETS_H_

#include "user_config.h"
#include "osapi.h"

// ---- define INPUT / OUTPUT TYPE ----------
#define IO_TERMO8	0      // -128..127
#define IO_INPUT	1      // 1/0
#define IO_OUT		2      // 1/0
#define IO_DIMM		3      // 0-99 
#define IO_HUMIDITY     4      // 0-99
#define IO_LUMINOSITY   5      // 0-99
#define IO_AIRQUALITY   6      // 0-99
#define IO_SMOKINESS    7      // 0-99
#define IO_DOOR         8      // 0/1
#define IO_VOLTSENSOR   9      // 0/1
#define IO_SIGNAL       10     // 0/1

#define IO_GASLEVEL     12     // 0/1
#define IO_MOTIONNT     13     // 0/1
#define IO_LEAKNT       14     // 0/1

#define IO_MOTION       50     // 0/1
#define IO_LEAK         51     // 0/1
#define IO_VOLTRESET    52     // 0/1
#define IO_SECURITY     53
#define IO_FIRELINE     54     // 0-100

#define IO_INPUT_ADC	100    // 0-1023
#define IO_COUNTER	101    // 0-
#define IO_IR_IN	102    // 0-
#define IO_IR_OUT	103    // 0-
#define IO_TERM 	104    // -127 - 128

#define IO_N2UNSGN0	110    // 0       .. 65535
#define IO_N2SGN0	111    // -32767  .. 32768
#define IO_N2UNSHN1	112    // 0.0     .. 6553.5
#define IO_N2SGN1	113    // -3276.7 .. 3276.8
#define IO_N2UNSGN2 	114    // 0.00    .. 655.35
#define IO_N2SGN2 	115    // -327.67 .. 327.68

#define IO_IEEE754FLT 	200    // -127 - 128

#define MAX_CHANNELS 255
short int channel_write [MAX_CHANNELS];
short int channel_len [MAX_CHANNELS];


//----------------------------------
struct channel {
    short int type;
    short int write;
    short int len;
    int data;
} esp8266_channels [CHANNELS];

void channels_init ();
void channels_parse (char * recive);
char * channels_generate (char* result);
char* itoa(int value, char* result, int base);

#define _PACKETS_H_
#endif