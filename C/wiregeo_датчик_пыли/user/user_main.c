#include "ets_sys.h"
#include "osapi.h"
//#include "gpio.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"
#include "packets.h"
#include "logic.h"
#include <mem.h>
#include "driver/uart.h"

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1

#include "espconn.h" 
LOCAL struct espconn *pCon = NULL;
//LOCAL my_count = 0;
//LOCAL your_value = 33;

 os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static volatile os_timer_t some_timer;
static volatile os_timer_t modbus_timer;

static void ICACHE_FLASH_ATTR
modbus_timerfunc(os_event_t *events)
{	
    uint8 buf_data[8];
    uint8 * buf;
    buf = & buf_data[0];
    buf_data[0] = 0x02;	//-a 2 
    buf_data[1] = 0x03;	// read reg 0x03
    buf_data[2] = 0x00;	// reg adr 0001
    buf_data[3] = 0x01;
    buf_data[4] = 0x00;	// reg count 0010
    buf_data[5] = 0x0A;
    buf_data[6] = 0x94;	// CRC-16 (Modbus)	0x3E94
    buf_data[7] = 0x3E;
	
    uart0_tx_buffer(buf, 8);
}

char * uart_data (char* send_data, int n){
	char FF[255];
	FF[0]='\0';
	char * str = FF;
	uint16 value = uartbuffer[n];
	value = value << 8;
	value = value | uartbuffer[n+1];
	
	uart_tx_one_char(UART1, uartbuffer[n]);
	uart_tx_one_char(UART1, uartbuffer[n+1]);
/*	
	if (uartbuffer[n] > 0){
	    itoa(uartbuffer[n], str, 10);
	    strcat(send_data,str);
	    FF[0]='\0';
	}
	itoa(uartbuffer[n+1], str, 10);
*/	
	itoa((int)value, str, 10);
	uart_tx_one_char(UART1, '=');
	uart_tx_one_char(UART1, str[0]);
	uart_tx_one_char(UART1, str[1]);
	uart_tx_one_char(UART1, '|');
	strcat(send_data,str);

	return send_data;
}

static void ICACHE_FLASH_ATTR networkRecvCb(void *arg, char *data, unsigned short len) {
 
  struct espconn *conn=(struct espconn *)arg;
  //os_printf("RECIVE:\r\n%s\r\nLEN: %d\r\n", data, len);
  data = strstr(data, "id=");
//  os_printf("RECIVE DATA: %s\r\n",data);

}

static void ICACHE_FLASH_ATTR publish_wiregeo_connect_cb(void *arg)
{
  //INFO("========>publish_wiregeo_connect_cb\r\n");

  struct espconn *pespconn = (struct espconn *)arg;

   char transmission[1024];
 
  char *header = "POST /sync_s.php HTTP/1.1\r\n"
                 "Host:  cloud.wiregeo.com\r\n"
                 "Accept: */*\r\n"
                 "User-Agent: esp8266\r\n"
		 "Connection: close\r\n"
                 "Content-Type: application/x-www-form-urlencoded\r\n"
                 "Content-Length: ";
  
  char json_data [1024];
  strcpy(json_data,"\0");
  char *json = json_data;
  strcat(json,"id=");
//	os_printf("channels_generate1: %s\r\n", json);
  strcat(json,ESP_ID);
//	os_printf("channels_generate2: %s\r\n", json);
  strcat(json,"&var_data={\"Sharp\":\"");
  uart_data (json, 3); // REG1
  strcat(json,"\",\"Samyong0\":\"");
  uart_data (json, 7); // REG3
  strcat(json,"\",\"Samyong1\":\"");
  uart_data (json, 9); // REG4
  strcat(json,"\",\"Samyong2\":\"");
  uart_data (json, 11); // REG5
  strcat(json,"\",\"Samyong3\":\"");
  uart_data (json, 13); // REG6
  strcat(json,"\"}");
  //channels_generate (json);
  strcat(json,"&hash=");
  strcat(json,"0");
  strcat(json,"\r\n");
//  os_printf("channels_generate3: %s\r\n", json);
  uart1_sendStr_no_wait(json);
	
  int head_len = strlen(header);
  int json_len = strlen(json);
 
  char json_len_str[10];
  os_sprintf(json_len_str,"%d",json_len);
  int json_len_str_len = strlen(json_len_str);
 
  strcpy(transmission,header);
  strcpy(transmission+head_len,json_len_str);
  strcpy(transmission+head_len+json_len_str_len,"\r\n\r\n");
  strcpy(transmission+head_len+json_len_str_len+4,json);
 
//  os_printf("%s\r\n", transmission);
//  os_printf("SEND: %s\r\n", json);
  
  espconn_regist_recvcb(pespconn, networkRecvCb);
  
  sint8 d = espconn_sent(pespconn,transmission,strlen(transmission));
  
//  os_printf("ERR: %d\r\n", d);
  uart1_sendStr_no_wait("ERR:");
  uart_tx_one_char(UART1, d);
  
}

void pCon_connect()
{
//    os_printf("Try espconn_connect\r\n");
    int ret = 0;	
    ret = espconn_connect(pCon);
	
    if(ret == 0) 
	//INFO("espconn_connect OK!\r\n");
//	os_printf("espconn_connect OK!\r\n");
	ret=0;
    else
    {
//	os_printf("espconn_connect FAILED (%d)!\r\n",ret);
	//clean up allocated memory
	if (pCon != NULL)
	{
	    if(pCon->proto.tcp)
                os_free(pCon->proto.tcp);
	    os_free(pCon);
	}
	pCon = NULL;
    } 
}

void some_timerfunc(void *arg)
{
    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
    if (pCon == NULL)
    {
//	os_printf("pCon NILL\r\n");
	if (wifi_station_get_connect_status () == STATION_CONNECTING)
	{
            pCon_connect();
	    pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
	}
	if (pCon == NULL)
	{
//	    os_printf("pCon ALLOCATION FAIL\r\n");
	    return;
	}
    }


    pCon->type = ESPCONN_TCP;
    pCon->state = ESPCONN_NONE;
    
    pCon->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
    pCon->proto.tcp->local_port = espconn_port();
    //set up the server remote port
    pCon->proto.tcp->remote_port = 80;

    //set up the remote IP
    uint32_t ip = ipaddr_addr("66.7.217.39"); //IP address for WireGEO.com
    os_memcpy(pCon->proto.tcp->remote_ip, &ip, 4);

    //set up the local IP
    struct ip_info ipconfig;
    wifi_get_ip_info(STATION_IF, &ipconfig);
    os_memcpy(pCon->proto.tcp->local_ip, &ipconfig.ip, 4);

    //register publish_wiregeo_connect_cb that will be called when the
    //connection with the wiregeo is done. In this call back function
    // will actualy do the data sending
    espconn_regist_connectcb(pCon, publish_wiregeo_connect_cb);
    
    pCon_connect();
}

//Do nothing function
static void ICACHE_FLASH_ATTR  user_procTask(os_event_t *events)
{
    //os_printf("*", system_get_sdk_version());
    os_delay_us(1000);
}

//Init function 
void ICACHE_FLASH_ATTR user_init()
{
//    uart_div_modify(0, UART_CLK_FREQ / 115200);
    system_set_os_print(0); //0/1 = debug off/on
	
    // Connect to Wi-Fi
//    os_printf("WireGEO cloud Wi-Fi connect.....\r\n");
//    os_printf("SDK version:%s\n", system_get_sdk_version());
	
    const char ssid[32] = SSID;
    const char password[32] = SSID_PASSWORD;

    struct station_config stationConf;

    wifi_set_opmode( STATION_MODE );
    os_memcpy(&stationConf.ssid, ssid, 32);
    os_memcpy(&stationConf.password, password, 32);
    wifi_station_set_config(&stationConf); 
    wifi_station_connect();

    // Initialize UART1 to use as debug
    uart_init(9600,115200);
    // Initialize UART0 to use as Modbus RTU
    UART_SetBaudrate	(UART0, BIT_RATE_9600); // 115200 
    UART_SetParity	(UART0, EVEN_BITS);	// even
    UART_SetWordLength	(UART0, EIGHT_BITS);	// 8
    UART_SetStopBits	(UART0, ONE_STOP_BIT);	// 1
	
    //connect to the previous pCon created structure
    pCon_connect();

    os_timer_disarm(&modbus_timer);	
    os_timer_setfn(&modbus_timer, (os_timer_func_t *)modbus_timerfunc, NULL);
    os_timer_arm(&modbus_timer, 1000, 1);
    
    os_timer_disarm(&some_timer);
    os_timer_setfn(&some_timer, (os_timer_func_t *)some_timerfunc, NULL);
    os_timer_arm(&some_timer, 5000, 1);
    
}