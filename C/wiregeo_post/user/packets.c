#include "packets.h"

void channels_init (){
    int n=0;
  /*  for (n=0; n<MAX_CHANNELS; n++){
	esp8266_channels[n].type = (-1);
    }*/
    
    if (CHANNELS > 0) esp8266_channels[0].type = (short int) CHANNEL00;
    if (CHANNELS > 1) esp8266_channels[1].type = (short int) CHANNEL01;
    if (CHANNELS > 2) esp8266_channels[2].type = (short int) CHANNEL02;
    if (CHANNELS > 3) esp8266_channels[3].type = (short int) CHANNEL03;
    if (CHANNELS > 4) esp8266_channels[4].type = (short int) CHANNEL04;
    if (CHANNELS > 5) esp8266_channels[5].type = (short int) CHANNEL05;
    if (CHANNELS > 6) esp8266_channels[6].type = (short int) CHANNEL06;
    if (CHANNELS > 7) esp8266_channels[7].type = (short int) CHANNEL07;
    if (CHANNELS > 8) esp8266_channels[8].type = (short int) CHANNEL08;
    if (CHANNELS > 9) esp8266_channels[9].type = (short int) CHANNEL09;
    if (CHANNELS > 10) esp8266_channels[10].type = (short int) CHANNEL0A;
    if (CHANNELS > 11) esp8266_channels[11].type = (short int) CHANNELAD;
	
//-------------------------------------------------------
    channel_write [IO_TERMO8]	= 0;      	// -128..127
    channel_write [IO_INPUT]	= 0;      	// 1/0
    channel_write [IO_OUT] 		= 1;      	// 1/0
    channel_write [IO_DIMM] 	= 1;      	// 0-100 
    channel_write [IO_HUMIDITY] 	= 0;     	// 0-100
    channel_write [IO_LUMINOSITY] 	= 0;    	// 0-100
    channel_write [IO_AIRQUALITY] 	= 0;      	// 0-100
    channel_write [IO_SMOKINESS] 	= 0;      	// 0-100
    channel_write [IO_DOOR] 	= 0;      	// 0/1
    channel_write [IO_VOLTSENSOR] 	= 0;      	// 0/1
    channel_write [IO_SIGNAL]	= 1;

    channel_write [IO_GASLEVEL] 	= 0;
    channel_write [IO_MOTIONNT] 	= 0;
    channel_write [IO_LEAKNT]	= 0;

    channel_write [IO_MOTION]	= 0;     	// 0/1
    channel_write [IO_LEAK] 	= 0;     	// 0/1
    channel_write [IO_VOLTRESET] 	= 1;     	// 0/1
    channel_write [IO_SECURITY] 	= 0;
    channel_write [IO_FIRELINE] 	= 1;     	// 0-100

    channel_write [IO_INPUT_ADC] 	= 0;    	// 0-1023
    channel_write [IO_COUNTER] 	= 1;    	// 0-
    channel_write [IO_IR_IN] 	= 0;    	// 0-
    channel_write [IO_IR_OUT]	= 1;    	// 0-
    channel_write [IO_TERM ] 	= 0;    // -127 - 128

    channel_write [IO_N2UNSGN0] 	= 1;      // 0       .. 65535
    channel_write [IO_N2SGN0] 	= 1;      // -32767  .. 32768
    channel_write [IO_N2UNSHN1] 	= 1;      // 0.0     .. 6553.5
    channel_write [IO_N2SGN1] 	= 1;      // -3276.7 .. 3276.8
    channel_write [IO_N2UNSGN2] 	= 1;      // 0.00    .. 655.35
    channel_write [IO_N2SGN2] 	= 1;      // -327.67 .. 327.68

    channel_write [IO_IEEE754FLT] 	= 1;      // -127 - 128
//---------------------------------------------------------

    channel_len [IO_TERMO8] 	=1;      // -128..127
    channel_len [IO_INPUT] 		=1;      // 1/0
    channel_len [IO_OUT] 		=1;      // 1/0
    channel_len [IO_DIMM] 		=1;      // 0-100 
    channel_len [IO_HUMIDITY]	=1;      // 0-100
    channel_len [IO_LUMINOSITY]	=1;      // 0-100
    channel_len [IO_AIRQUALITY]	=1;      // 0-100
    channel_len [IO_SMOKINESS]	=1;      // 0-100
    channel_len [IO_DOOR]		=1;      // 0/1
    channel_len [IO_VOLTSENSOR]	=1;      // 0/1
    channel_len [IO_SIGNAL]		=1;

    channel_len [IO_GASLEVEL]	=1;
    channel_len [IO_MOTIONNT]	=1;
    channel_len [IO_LEAKNT]		=1; 

    channel_len [IO_MOTION]		=1;     // 0/1
    channel_len [IO_LEAK]		=1;     // 0/1
    channel_len [IO_VOLTRESET]	=1;     // 0/1
    channel_len [IO_SECURITY]	=1;
    channel_len [IO_FIRELINE]	=1;     // 0-100

    channel_len [IO_INPUT_ADC] 	=2;    // 0-1023
    channel_len [IO_COUNTER] 	=2;    // 0-
    channel_len [IO_IR_IN] 		=0;    // 0-
    channel_len [IO_IR_OUT] 	=0;    // 0-
    channel_len [IO_TERM ] 		=2;    // -127 - 128

    channel_len [IO_N2UNSGN0] 	=2;    // 0       .. 65535
    channel_len [IO_N2SGN0] 	=2;    // -32767  .. 32768
    channel_len [IO_N2UNSHN1] 	=2;    // 0.0     .. 6553.5
    channel_len [IO_N2SGN1] 	=2;    // -3276.7 .. 3276.8
    channel_len [IO_N2UNSGN2] 	=2;    // 0.00    .. 655.35
    channel_len [IO_N2SGN2 ] 	=2;    // -327.67 .. 327.68

    channel_len [IO_IEEE754FLT]	=4;    // -127 - 128
//--------------------------------------------------------

    for (n=0; n<CHANNELS; n++){
	esp8266_channels[n].write =     channel_write [ esp8266_channels[n].type ];
	esp8266_channels[n].len =     channel_len [ esp8266_channels[n].type ];
    }
}

void channels_parse (char * recive){
    os_printf("data recive: %s\r\n", recive);
    os_printf("data LEN: %d\r\n", strlen(recive));
	  
    char FF[255];
    FF[2]='\0';
	
    int data_len = 0;
    int data = 0;
    int n,m= 1;
    int type = 0;
    char * hexstring = FF;
    int number =0;
    for (n=5 ; n < strlen(recive); n){
	FF[0] = recive[n+2]; // номер канала
	FF[1] = recive[n+3];
	FF[2]='\0';
	number = (int)strtol(hexstring, NULL, 16);
	//os_printf("%d: channel number= %s = %d\r\n", n, hexstring, number);
	    
	FF[0] = recive[n];	// тип канала
	FF[1] = recive[n+1];
	FF[2]='\0';
	n += 4;
	type = (int)strtol(hexstring, NULL, 16);
	//os_printf("%d: channel type= %s = %d\r\n", n, hexstring, type);

	if (channel_len [type] == 0){
	    FF[0] = recive[n]; // длинна данных канала
	    FF[1] = recive[n+1];
	    FF[2]='\0';
	    data_len = (int)strtol(hexstring, NULL, 16);
	    n +=2;
	}
	else
	    data_len = channel_len [type];
	//os_printf("%d: channel len= %d byte []=%d byte\r\n", n, data_len, channel_len [type]);
	data_len *=2;
	
	for (m=n; m<data_len+n ;m++)
	    FF[m-n] = recive[m];
	FF[data_len]='\0';
	n +=data_len;
	//os_printf("%d: channel data= %s\r\n", n, hexstring);
	
	if (esp8266_channels [number].type ==  type && esp8266_channels [number].type > -1) {
	    os_printf("channel number=%d id=%d len= %d data = %d\r\n", number, esp8266_channels [number].type, data_len, data);
	    esp8266_channels [number].data = (int)strtol(hexstring, NULL, 16);
	}
	else
	    os_printf("channel number=%d  id=%d != %d ERR\r\n", number, esp8266_channels [number].type , type);
	
    } 

}

char * channels_generate (char* send_data){
	int n=0;
	int i = 0;
	//char send_data [256];
	//char * data = send_data;
	char FF[255];
	FF[0]='\0';
	char * hexstring = FF;
	
	for (n=0;n<CHANNELS; n++){
	    itoa(esp8266_channels [n].type, hexstring, 16);
	    if ((int)strlen(hexstring)==1) strcat(send_data,"0\0");
	    strcat(send_data,hexstring);
	    //os_printf("%d: channel type=%d = %sh len=%d\r\n", n,esp8266_channels [n].type, hexstring, strlen(hexstring));
	    itoa(n, hexstring, 16);
	    if ((int)strlen(hexstring)==1) strcat(send_data,"0\0");
	    strcat(send_data,hexstring);
	    //os_printf("%d: channel number=%d = %sh len=%d\r\n", n, n, hexstring, strlen(hexstring));
	    itoa(esp8266_channels [n].data, hexstring, 16);
	    if ((int)strlen(hexstring)<esp8266_channels [n].len*2)
		for (i=0; i < esp8266_channels [n].len*2-strlen(hexstring);i++)
			strcat(send_data,"0\0");
	    strcat(send_data,hexstring);
	    //os_printf("%d: channel data=%d = %sh len=%d\r\n", n, esp8266_channels [n].data, hexstring, strlen(hexstring));
	    //os_printf("%d: resulr data= %s len=%d\r\n", n, send_data, strlen(send_data));
	}
	return send_data;
}

void reverse(char* begin, char* end) {
    char *is = begin;
    char *ie = end - 1;
    while(is < ie) {
        char tmp = *ie;
        *ie = *is;
        *is = tmp;
        ++is;
        --ie;
    }
}

char* itoa(int value, char* result, int base) {
    if(base < 2 || base > 16) {
        *result = 0;
        return result;
    }

    char* out = result;
    int quotient = abs(value);

    do {
        const int tmp = quotient / base;
        *out = "0123456789abcdef"[quotient - (tmp * base)];
        ++out;
        quotient = tmp;
    } while(quotient);

    // Apply negative sign
    if(value < 0)
        *out++ = '-';

    reverse(result, out);
    *out = 0;
    return result;
}