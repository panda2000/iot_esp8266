#include "publish_wiregeo.h"

#define ESP_ID "ESP-01"

void ICACHE_FLASH_ATTR publish_wiregeo_connect_cb(void *arg)
{
  //INFO("========>publish_wiregeo_connect_cb\r\n");

  struct espconn *pespconn = (struct espconn *)arg;

  char transmission[1024];
  char api_key [17];
  os_sprintf(api_key, "%s", "9RPEAG5SJMP3VQBX");
 
  char *header = "POST /sync_s.php HTTP/1.1\r\n"
                 "Host:  cloud.wiregeo.com\r\n"
                 "Accept: */*\r\n"
                 "User-Agent: esp8266\r\n"
		 "Connection: close\r\n"
                 "Content-Type: application/x-www-form-urlencoded\r\n"
                 "Content-Length: ";
  
  char json_data [1024];
  strcpy(json_data,"\0");
  char *json = json_data;
  strcat(json,"id=");
	os_printf("channels_generate1: %s\r\n", json);
  strcat(json,ESP_ID);
	os_printf("channels_generate2: %s\r\n", json);
  strcat(json,"&var_data={\"Lamp1\":\"0\",\"Temperature1\":\"28.4\"}");
  
  //---------------------------------------------------------
  MD5_CTX context;
  unsigned char digest[16];
  int n=0;
  char FF[3];
  FF[0]='\0';
  char * hexstring = FF;

  char hash_st [2048];
  hash_st[0] = '\0';
  char *hash_st_p = hash_st;
  strcat(hash_st_p,"WIREGEO-");
  strcat(hash_st_p,json);
  strcat(hash_st_p,"&-");
  strcat(hash_st_p,api_key);
  strcat(hash_st_p,"-WG");

  os_printf("STR : %s len= %d\r\n", hash_st_p, strlen (hash_st_p));

  MD5Init (&context);
  MD5Update (&context, hash_st_p, strlen (hash_st_p));
  MD5Final (digest, &context);
 
   strcat(json,"&hash=");
  for (n=0;n<16; n++){
    itoa(digest[n], hexstring, 16);
//    os_printf("%d Hash : %d = %s len=%d\r\n", n, digest[n], hexstring, strlen (hexstring));
    if (strlen (hexstring) == 1)
	strcat(json,"0");
    strcat(json,hexstring);
  }
  //strcat(json,"\r\n");
  os_printf("channels_generate3: %s\r\n", json);
  //---------------------------------------------------------
	
  int head_len = strlen(header);
  int json_len = strlen(json);
 
  char json_len_str[10];
  os_sprintf(json_len_str,"%d",json_len);
  int json_len_str_len = strlen(json_len_str);
 
  strcpy(transmission,header);
  strcpy(transmission+head_len,json_len_str);
  strcpy(transmission+head_len+json_len_str_len,"\r\n\r\n");
  strcpy(transmission+head_len+json_len_str_len+4,json);
 
  //os_printf("%s\r\n", transmission);
  os_printf("SEND: %s\r\n", json);
  
  espconn_regist_recvcb(pespconn, recv_wiregeo_cb);
  
  sint8 d = espconn_sent(pespconn,transmission,strlen(transmission));
  
  os_printf("ERR: %d\r\n", d);	
  
}


static void ICACHE_FLASH_ATTR recv_wiregeo_cb(void *arg, char *data, unsigned short len) {
 
  struct espconn *conn=(struct espconn *)arg;
  //os_printf("RECIVE:\r\n%s\r\nLEN: %d\r\n", data, len);
  //data = strstr(data, "id=");
  os_printf("RECIVE DATA: %s\r\n",data);
/*	
  if (data != NULL){
    int x = strcspn(data,"&");
    char id[32];
    char data_[512];
    char dt[10];
    strncpy(id,data,x);
    id[x+1]='\0';
    os_printf("id: %s  %d\r\n", id, x);
	
    data = strstr(data, "data=");
    x = strcspn(data,"&");
    strncpy(data_,data,x);
    data_[x+1]='\0';
//    channels_parse (data_);
	
    data = strstr(data, "dt=");
    os_printf("data: %s\r\n", data);
  }
*/
}